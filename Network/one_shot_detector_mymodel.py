# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np


CLASS_NUM = 2


def ones(shape, name="none"):
    val = np.ones(shape, dtype=np.float32)*(1./(shape[0]*shape[1]*shape[2]))
    return tf.Variable(val, name=name)


class HeInitializer:
    """
    ObjectDetectionNNにおいて重みの初期化に使用される
    """
    def __init__(self):
        self.initializer = tf.variance_scaling_initializer()

    def __call__(self, shape, name):
        print(shape)
        var = tf.Variable(self.initializer(shape=shape), name=name)
        return var


def low_value(shape, name):
    """
    ObejectDetectionNNにおいてバイアスの初期化に使用される
    :param shape:
    :param name:
    :return:
    """
    init = tf.constant(0.1, shape=shape)
    return tf.Variable(init, name=name)


class ObjectDetectionNN:
    """
    Recognition Model　及び　Regression Modelのネットワーク構築を行う
    """

    def __init__(self):
        self.weights_initializer = HeInitializer()
        self.bias_initializer = low_value
        self.detection_vars = []
        self.regression_vars = []
        self.mode = False
        self.scope = 'convnet'
        self.layers = []
        self.net = None
        self.num_classes = CLASS_NUM

    def __call__(self, inputs, reuse=False):
        """
        インスタンスを生成した段階でRecognition Modelを生成する
        :param inputs:
        :param reuse:
        :return:
        """
        self.reuse = reuse
        return self.conv_network(images=inputs)

    def add(self, layer):
        if self.net is None:
            self.net = layer
        else:
            self.net = layer(self.net)

        if isinstance(layer, tf.keras.layers.Layer):
            self.layers.append(layer)

    def get_weights(self):
        weights = []
        for l in self.detection_vars:
            weights.append(l.get_weights())

        return weights

    def set_weights(self, weights):
        for i in range(len(self.detection_vars)):
            self.detection_vars[i].set_weights(weights[i])


    def changer(self):
        """

        :return:
        """
        self.mode = True
        self.scope = 'convnet2'

    # def conv2d(self, input, kernel_size, stride, channels, name, padding="VALID", activation=tf.nn.relu):
    #     input_ch = tf.shape(input.shape)[3]
    #     kernel = self.weights_initializer([kernel_size[0], kernel_size[1], input_ch, channels], name=name+"_kernel")
    #     bias = self.bias_initializer(channels, name=name+"_bias")
    #     conv = tf.nn.conv2d(input, kernel, strides=[1, stride[0], stride[1], 1], padding=padding) + bias
    #     conv = activation(conv, name=name+"_activation")
    #
    #     return conv, kernel, bias

    def conv2d(self, inputs, kernel_size, strides, filters, name, padding="VALID", activation=tf.nn.relu):
        """
        畳み込み層ーの実装が成されている
        :param inputs:
        :param kernel_size:
        :param strides:
        :param filters: Integer, the dimensionality of the output space (i.e. the number of filters in the convolution).
        :param name:
        :param padding:
        :param activation:
        :return:
        """
        layer = tf.layers.Conv2D(filters, kernel_size, strides, padding, activation=activation, _reuse=self.reuse,
                                 name=name)
        out = layer(inputs)

        return out

    def dense(self, inputs, n_outputs, name, activation=tf.nn.relu):
        """
        全結合層の実装が成されている
        :param inputs:
        :param n_outputs:
        :param name:
        :param activation:
        :return:
        """
        # W = self.weights_initializer([n_outputs], name=name+"_w")

        # return tf.matmul(input, W)+ B, W, B

        return tf.layers.dense(inputs, n_outputs, activation=activation,
                               #kernel_initializer=self.weights_initializer,
                              #bias_initializer=self.bias_initializer,
                               name=name)

    def conv_network4(self, images):
        with tf.variable_scope('convnet'):

            net = self.conv2d(images, [5, 5], [1, 1], 64, "conv1")
            net = tf.layers.max_pooling2d(net, [2, 2], [2, 2], "valid")
            net = self.conv2d(net, [3, 3], [1, 1], 32, "conv2")
            net = tf.layers.max_pooling2d(net, [2, 2], [2, 2], "valid")
            net = self.conv2d(net, [3, 3], [1, 1], 32, "conv3")
            net = tf.layers.max_pooling2d(net, [2, 2], [2, 2], "valid")
            net = self.conv2d(net, [6, 6], [1, 1], 64, "conv4")
            net = self.conv2d(net, [1, 1], [1, 1], 64, "conv5")
            net = self.conv2d(net, [1, 1], [1, 1], 2, "conv6")
        self.detection_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='convnet')
        # print(self.detection_vars)
        return net

    def conv_network(self, images):
        with tf.variable_scope('convnet'):
            self.add(images)
            self.add(tf.keras.layers.Conv2D(64, [5, 5], [1, 1]))
            self.add(tf.keras.layers.MaxPool2D([2, 2], [2, 2]))
            self.add(tf.keras.layers.Conv2D(32, [3, 3], [1, 1]))
            self.add(tf.keras.layers.MaxPool2D([2, 2], [2, 2]))
            self.add(tf.keras.layers.Conv2D(32, [3, 3], [1, 1]))
            self.add(tf.keras.layers.MaxPool2D([2, 2], [2, 2]))
            self.add(
                tf.keras.layers.Conv2D(64, [6, 6], [1, 1]))  # これでちょうど[?,1,1,?]となるように，入力サイズを変えたらkernelを調整して[?,1,1,?]とする
            self.add(tf.keras.layers.Conv2D(64, [1, 1], [1, 1]))  # full connect
            self.add(tf.keras.layers.Conv2D(self.num_classes, [1, 1], [1, 1]))  # full connect

        #        self.detection_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='convnet')
        self.detection_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope="conv_layers")
        # print(self.detection_vars)
        return self.net

    def conv_network2(self, images):

        """
        転移学習なし
        :param images:
        :return:
        """

        with tf.variable_scope('convnet'):
            net = self.conv2d(images, [5, 5], [1, 1], 64, "conv1")  # input,kernel,stride,channels
            net = tf.layers.max_pooling2d(net, [2, 2], [2, 2], "valid")
            net = self.conv2d(net, [3, 3], [1, 1], 32, "conv2")
            net = tf.layers.max_pooling2d(net, [2, 2], [2, 2], "valid")
            net = self.conv2d(net, [3, 3], [1, 1], 16, "conv3")
            net = tf.layers.max_pooling2d(net, [2, 2], [2, 2], "valid")

            #        net = self.conv2d(net,    [3,3], [1,1],256, "conv4")

            net = self.conv2d(net, [6, 6], [1, 1], 8, "fc1")
            print(net)
            net = self.conv2d(net, [1, 1], [1, 1], 2, "fc2")

            #net = tf.layers.flatten(heatmap)
            #net = self.dense(net, 8, "fc2")
            #final_output_for_check_accuracy = self.dense(net, 2, "fc3")

        self.detection_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='convnet')
        # print(self.detection_vars)
        return net

    def conv_network3(self, images):
        """
　　　　　転移学習あり
        VGG19の入力は4次元である必要がある
        :param images:
        :return:
        """
        reuse = self.reuse
        name = 'test'
        freeze = True

        # 初期設定はTrueになっている。変更するにはself.changer()を呼ぶことでFalseに変換
        with tf.variable_scope("VGG19" + name, reuse=reuse) as scope:

            if reuse:
                scope.reuse_variables()

            with tf.variable_scope("VGG19"):
                print("in:{}".format(images.shape))
                inputs = images#tf.expand_dims(images[0], 0)
                print(inputs.shape)
                vgg19 = tf.keras.applications.VGG19(include_top=False,
                                                    weights='imagenet',
                                                    input_shape=(inputs.shape[1], inputs.shape[2], 3),#(64, 64, 3),
                                                    input_tensor=inputs)
                if freeze:  # 下位層のみ学習する場合
                    for vgg_layer in vgg19.layers:
                        vgg_layer.trainable = False

                # vgg19の4層目にあたる出力を取得する
                vgg19_out = tf.identity(vgg19.layers[-20].output, name='output')
                print("name:{}".format(vgg19_out))
                print("transposed_shape:{}".format(vgg19_out.shape))

            with tf.variable_scope("my_convolution"):

                # vgg19_outの入力shapeは(?, 64, 64, 3)に変更すること
                net = self.conv2d(inputs=vgg19_out, kernel_size=[3, 3], strides=[1, 1], filters=64, name="conv1")
                net = self.conv2d(net, kernel_size=[3, 3], strides=[1, 1], filters=64, name="conv2")

                net = tf.layers.max_pooling2d(net, pool_size=[2, 2], strides=[2, 2], padding="same")
                #net = tf.nn.dropout(net, keep_prob=0.7, name="dropout")

                net = self.conv2d(net, kernel_size=[3, 3], strides=[1, 1], filters=128, name="conv3")
                net = self.conv2d(net, kernel_size=[3, 3], strides=[1, 1], filters=128, name="conv4")
                heat_map = tf.layers.max_pooling2d(net, pool_size=[2, 2], strides=[1, 1], padding="same")
                heat_map = tf.identity(heat_map, name="heat_map")
                #net = self.conv2d(net, kernel_size=[3, 3], strides=[1, 1], filters=8, name="fc1")
                #net = tf.layers.max_pooling2d(net, pool_size=[2, 2], strides=[1, 1], padding="valid")

                #net = self.conv2d(net, kernel_size=[1, 1], strides=[1, 1], filters=2, name="fc2")


                # (1,25,25,2)になる必要がある
#            if mode:
            #    net = tf.layers.flatten(heat_map)
            #    net = self.dense(net, 512, name="fc5")
            #    net = self.dense(net, 2, name="fc6")

        # tf.get_collectionにより第二引数にscopeを指定することで、特定のscopeのものだけを取得する
        # https://qiita.com/n_kats_/items/1145e7320f241495d868
        self.detection_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="VGG19" + name)
        return heat_map

    def reg_network(self, heatmap):
        """
        中心点を回帰するRegression Modelのネットワークを構築する
        :param heatmap:
        :return:
        """
        activation = tf.nn.relu

        with tf.variable_scope("regression"):
            net = tf.layers.conv2d(heatmap, filters=16, kernel_size=[3, 3], strides=[1, 1], padding="SAME",
                                   activation=activation, name="reg_conv1")

            net = tf.layers.conv2d(net, filters=8, kernel_size=[3, 3], strides=[1, 1], padding="SAME",
                                   activation=activation, name="reg_conv2")

            net = tf.layers.flatten(net)
            net = tf.layers.dense(net, 16, activation, "reg_fc1")
            net = tf.layers.dense(net, 16, activation, "reg_fc2")
            net = tf.layers.dense(net, 2, activation, "reg_fc3")

        # tf.get_collectionにより第二引数にscopeを指定することで、特定のscopeのものだけを取得する
        self.regression_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='regression')
        return net


