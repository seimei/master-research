# -*- coding: utf-8 -*-

from enum import Enum
import InputPipline.DistTest


class TrainMode(Enum):
    train = 0
    regression = 1
    test = 2


# 切り替え部分
TRAIN_MODE = TrainMode.regression


if __name__ == '__main__':

    # Recognition Modelの学習
    if TRAIN_MODE == TrainMode.train:
        InputPipline.DistTest.DistTest().run()

    # Regression　Modelの学習
    elif TRAIN_MODE == TrainMode.regression:
        InputPipline.DistTest.DistTest().run_regression()

    # test
    else:
        InputPipline.DistTest.DistTest().run_test()


