
import tensorflow as tf
import cv2
import numpy as np
import math
from scipy.stats import norm
import noise
import math
from Network.one_shot_detector_mymodel import ObjectDetectionNN


from numpy import random

SAVE_MODEL = True
READ_MODEL = False
MODEL_PATH = ""

LOG_DIR = 'logs'

# 訓練データCSV
TRAIN_FILE = 'path_and_label_train.csv'#'path_and_label_concat.csv'#'path_and_label_train.csv'
# テストデータ
DETECT_FILE = 'path_and_label_test.csv'

VALIDATION_FILE = 'path_and_label_valid.csv'

TRAIN_IMAGE_DIR =""
# 訓練データ
DETECT_IMAGE_DIR =""

CNN_MODEL_SAVE_NAME = "test/main"
REG_MODEL_SAVE_NAME = "test/reg"

# どのepochのmodelを使うか
CNN_MODEL_READ_STEP = 400
REG_MODEL_READ_STEP = 200

# テスト時に画像を変換
INPUT_IMAGE_PRE_RESIZE = 400

INPUT_IMAGE_SIZE = 256

INPUT_IMAGE_CHANNELS = 3

# crop resize時の画像サイズ
WINDOW_SIZE = 64#128
# クラス数
NUM_CLASSES = 2

# CNNの訓練時
BATCH_SIZE = 64# 256
N_READER_THREADS = 32#tf.data.experimental.AUTOTUNE#32#8



REGRESSION_HEATMAP_SIZE = 25

REGRESSION_BATCH_SIZE = 256


gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
config = tf.ConfigProto(device_count={'GPU': 0, 'CPU': 56}, gpu_options=gpu_options)


def count_samples(filename):
    """
    訓練データの画像数をカウントする
    :param filename:
    :return:
    """
    potA = 0
    potC = 0
    count = 0
    with open(filename, "r") as f:
        for i in f:
            num = i.split(",")
            if num[-1] == "1\n":
                potC += 1
            else:
                potA += 1
            count += 1

    return count, potA, potC


train_data_size, potA, potC = count_samples(TRAIN_FILE)
print("number_of train_data=", train_data_size)

print("number_of_potC:", potC)
print("nunmber_of_potA", potA)
#valid_data_size = count_samples(VALIDATION_FILE)
#print("number_of valid_data=", valid_data_size)

#test_data_size = count_samples(DETECT_FILE)
#print("number of test_data=", test_data_size)

class DistTest:
    """
    訓練データ及び検証データをRecognition Model及びRegression Modelに流す操作を保持するクラス
    """

    def __init__(self):
        pass
        # self.initializer = ones

    def input_pipeline(self, filepath, image_dir, image_size, crop_size,
                       batch_size, mode_train=True, validation_filepath=None):

        """
        訓練データ及び検証データをRecognition Model及びRegression Modelに流すパイプの構築
        csvよりpath及びlabelを読み込み、tf.data.TextLineDatasetを使用している。
        :param filepath: 訓練・検証データへのpath
        :param image_dir:
        :param image_size:
        :param crop_size:
        :param batch_size:
        :param mode_train:
        :return: images <class 'tensorflow.python.framework.ops.Tensor'>, tf.cast(flag, tf.int32), input_iter(イテレータ)
        """

        def parse_cvs(line):
            return tf.decode_csv(line, [["no file"], [0.]])


        def read_image(filename):
            jpeg_r = tf.read_file(image_dir + filename)
            image = tf.image.decode_jpeg(jpeg_r, channels=INPUT_IMAGE_CHANNELS)  # rgb
            return image


        def resize(image):
            """
            画像を指定したmethodで(new_height,new_width)にリサイズ
            :param image:
            :return:
            """
            # asp = image.shape[0]/image.shape[1]
            return tf.image.resize_images(image, (image_size, image_size),
                                          method=tf.image.ResizeMethod.AREA,
                                          preserve_aspect_ratio=True)
            # preserve_aspect_ratio=True)

        def fit_size(image):
            """
            画像を指定したサイズ(target_height,target_width)になるよう
            トリミングand/orパディングする
            :param image:
            :return:
            """
            resized = tf.image.resize_image_with_crop_or_pad(image,
                                                             crop_size, crop_size)
            return resized

        def normalize(image):
            """
            画像の正規化を行う
            :param image:
            :return:
            """
            return tf.cast(image, tf.float32) * (1. / 256)

        # Data Augmentation
        def random_crop(image):
            """
            画像からrandom_cropを行う
            :return:
            """
            W = tf.minimum(crop_size, tf.shape(image)[0])
            H = tf.minimum(crop_size, tf.shape(image)[1])
            resized = tf.image.random_crop(image, [W,H, INPUT_IMAGE_CHANNELS])
            return resized

        def flip_left_and_right(image):
            """
　　　　　　　画像を反転させる
            :param image:
            :return:
            """
            return tf.image.flip_left_right(image)

        def image_rot(image, k=1, name=None):
            """
            画像を回転
            :return:
            """
            return tf.image.rot90(image, k, name)

        dataset_valid = tf.data.TextLineDataset(validation_filepath) \
            .skip(1) \
            .shuffle(100) \
            .map(parse_cvs) \
            .map(lambda file, gt_flag: (read_image(file), gt_flag)) \
            .map(lambda im, gt_flag: (resize(im), gt_flag)) \
            .map(lambda im, gt_flag: (random_crop(im), gt_flag)) \
            .map(lambda im, gt_flag: (fit_size(im), gt_flag)) \
            .map(lambda im, gt_flag: (normalize(im), gt_flag))

        if mode_train:
            dataset = tf.data.TextLineDataset(filepath) \
                .skip(1) \
                .shuffle(100) \
                .map(parse_cvs) \
                .map(lambda file, gt_flag: (read_image(file), gt_flag)) \
                .map(lambda im, gt_flag: (resize(im), gt_flag)) \
                .map(lambda im, gt_flag: (random_crop(im), gt_flag)) \
                .map(lambda im, gt_flag: (fit_size(im), gt_flag)) \
                .map(lambda im, gt_flag: (normalize(im), gt_flag))

        else:
            dataset = tf.data.TextLineDataset(filepath) \
                .skip(1) \
                .map(parse_cvs) \
                .map(lambda file, gt_flag: (read_image(file), gt_flag)) \
                .map(lambda im, gt_flag: (resize(im), gt_flag)) \
                .map(lambda im, gt_flag: (fit_size(im), gt_flag)) \
                .map(lambda im, gt_flag: (normalize(im), gt_flag))

        # 訓練用
        dataset = dataset.batch(batch_size=batch_size).shuffle(100)
        dataset = dataset.prefetch(buffer_size=N_READER_THREADS)
        # 検証用
        dataset_valid = dataset_valid.batch(batch_size=batch_size).shuffle(100)
        dataset_valid = dataset_valid.prefetch(buffer_size=N_READER_THREADS)

        # 訓練用
        #        self.input_iter = dataset.make_initializable_iterator()
        input_iter = dataset.make_initializable_iterator()
        # batchごとにデータを流す
        images, flag = input_iter.get_next()
        #        crop = tf.image.crop_to_bounding_box(images, shape[1],shape[2],window_size,window_size)
        #        image = crop

        # 検証用
        input_iter_valid = dataset_valid.make_initializable_iterator()
        # batchごとにデータを流す
        images_valid, flag_valid = input_iter_valid.get_next()

        print('image_type:{}'.format(type(images)))
        return images, tf.cast(flag, tf.int32), input_iter, images_valid, tf.cast(flag_valid, tf.int32), input_iter_valid

    def tensorToRGB(self, tensor):
        """
        恐らく検証結果を可視化する際に使用する show2()/show()で使用されている
        :param tensor:
        :return:
        """
        if tensor.ndim == 2 or tensor.shape[2] == 1:
            col = np.ndarray(shape=[tensor.shape[0], tensor.shape[1], 3], dtype=tensor.dtype)
            col[:, :, 0:1] = tensor
            col[:, :, 1:2] = tensor
            col[:, :, 2:3] = tensor
            return col
        elif tensor.shape[2] == 3:
            return cv2.cvtColor(tensor, cv2.COLOR_BGR2RGB)
        elif tensor.shape[2] == 2:
            zeros = np.zeros(shape=(tensor.shape[0], tensor.shape[1], 1), dtype=np.float32)
            return np.append(tensor, zeros, axis=2)
        else:
            print("invalid tensor format for showing")
            exit()

    def show2(self, batcha, batchb):
        resx = [self.tensorToRGB(i) for i in batcha]
        rest = [self.tensorToRGB(i) for i in batchb]
        cv2.imshow("show", cv2.hconcat([cv2.vconcat(resx), cv2.vconcat(rest)]))

    #        cv2.waitKey(wait)

    def show(self, batcha, name="show"):
        resx = [self.tensorToRGB(i) for i in batcha]
        resized = cv2.resize(cv2.vconcat(resx), (360, 360))
        cv2.imshow(name, resized)
        cv2.waitKey(1)

    def show_tile(self, batch, name="show"):
        ilist = [self.tensorToRGB(i) for i in batch]

        w = round(math.sqrt(batch.shape[0]) + 0.5)
        h = round(batch.shape[0] / w + 0.4)
        sh = ilist[0].shape
        extra = [np.zeros(shape=sh, dtype=ilist[0].dtype) for _ in range(w * h - len(ilist))]
        ilist.extend(extra)

        tile = []
        for r in range(0, w * h, w):
            row = []
            for c in range(w):
                row.append(ilist[r + c])

            tile.append(cv2.hconcat(row))

        # print(cv2.vconcat(tile).shape)
        cv2.imshow(name, cv2.vconcat(tile))
        cv2.waitKey(1)

    def to_label(self, img, shape):
        """

        :param img:
        :param shape:
        :return:
        """
        gt_ = cv2.resize(img, shape, None, 0, 0, cv2.INTER_AREA)
        gt_ = gt_[:, :, 0:1]
        ngt = 1 - gt_
        gt_ = np.append(gt_, ngt, axis=2)

        return gt_

    def run(self):
        """
        Recognition Model 及び Regression Modelを学習させる
        :return:
        """

        # self.input_piplineはtf.data.TextLineDatasetを使用している。画像(parse済み)・ラベル・イテレータを返す
        # モデルの訓練用
        img, label, train_iter, img_valid, label_valid, train_iter_valid = self.input_pipeline(filepath=TRAIN_FILE,
                                                                                               image_dir=TRAIN_IMAGE_DIR,
                                                                                               image_size=WINDOW_SIZE,  # 画像のresizeに使用
                                                                                               crop_size=WINDOW_SIZE,  # 画像のcropに使用
                                                                                               batch_size=BATCH_SIZE,   # 256
                                                                                               mode_train=True,
                                                                                               validation_filepath=VALIDATION_FILE)
        # Recognition Modelを作成
        net = ObjectDetectionNN()

        x = img
        # ラベルをone_hot表現に変換する
        t = tf.one_hot(label, NUM_CLASSES)
        n = net(x)

        # バッチサイズを取得
        batch_size = tf.shape(n)[0]  # (?, 1, 1, 2)

        # Recognition Modelにはそもそも全結合層が無い説
        # 無理矢理次元を落とし、Softmaxを算出っぽい

        y = tf.reshape(n, [batch_size, 2])
        # Recognition Modelの出力をsotfmax

        #flat = tf.layers.flatten(n)
        #y = tf.layers.dense(flat, 8)
        #y = tf.layers.dense(y, 2)

        y = tf.nn.softmax(y)

        # Recognition Modelの損失を取得
        loss = tf.losses.softmax_cross_entropy(t, y)

        # tf.equal(x, y) →x==yの真理値を返す
        # tf.argmax(y, 1) →リストyから最大の要素を求める1は軸方向(axis=0:縦, axis=1:横)
        # tf.cast(変更前のリスト, 変更後の型) →型の変更に用いる（bool型をfloat型などに変更）

        # 予測中何個が合っているのか計算
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.cast(label, tf.int64))

        # tf.reduce.mean() →与えられたリストのすべての要素の平均
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        # Adamで損失を抑え学習を行う
        train_step = tf.train.AdamOptimizer(1e-4).minimize(loss)

        #################################################################################################

        y_valid = net(img_valid, True)

        # バッチサイズを取得
        batch_size_valid = tf.shape(y_valid)[0]  # (?, 1, 1, 2)

        # Recognition Modelにはそもそも全結合層が無い説
        # 無理矢理次元を落とし、Softmaxを算出っぽい

        y_valid = tf.reshape(y_valid, [batch_size_valid, 2])
        # Recognition Modelの出力をsotfmax

        #flat = tf.layers.flatten(y_valid)
        #y = tf.layers.dense(flat, 8)
        #y_valid = tf.layers.dense(y, 2)


        y_valid = tf.nn.softmax(y_valid)

        t_val = tf.one_hot(label_valid, NUM_CLASSES)
        # Recognition Modelの損失を取得
        loss_valid = tf.losses.softmax_cross_entropy(t_val, y_valid)

        # 予測中何個が合っているのか計算
        correct_prediction_valid = tf.equal(tf.argmax(y_valid, 1), tf.cast(label_valid, tf.int64))

        # tf.reduce.mean() →与えられたリストのすべての要素の平均
        accuracy_valid = tf.reduce_mean(tf.cast(correct_prediction_valid, tf.float32))


        ###################################################################################################

        # (可視化用)→訓練時どの領域にヒートマップが出ているのか可視化するためのInput Pipeline
        detect_img, _, detect_iter, _, _, _ = self.input_pipeline(filepath=DETECT_FILE,
                                                                  image_dir=DETECT_IMAGE_DIR,
                                                                  image_size=INPUT_IMAGE_SIZE,
                                                                  crop_size=INPUT_IMAGE_SIZE,
                                                                  batch_size=1,
                                                                  mode_train=True,
                                                                  validation_filepath=VALIDATION_FILE)

        test_image_path = ["/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/587.jpg"]
                          # "/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/1077.jpg",
                          # "/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/428.jpg",
                          # "/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/1406.jpg"]

        test_image = 0
        test_images = []
        for path in test_image_path:
            test_image = cv2.imread(path)
            test_images.append(test_image)
            # 画像の
            test_image = np.stack(test_images)

        detect_input = tf.placeholder(tf.float32, [None, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, 3])
        detect_input2 = tf.placeholder(tf.float32, [None, 405, 720, 3])
        ########################
        # Recognition Modelの作成
        ########################

        heat_map_net = net(detect_input2, True)


        # Recognition Modelの出力を取得
        heat_map = heat_map_net[:, :, :, 1:2]  # オブジェクトラベルを取得

        # 配列の中より最大値を取得することで出力値のヒートマップ化
        max1 = tf.reduce_max(heat_map, axis=None, keepdims=True)
        # 正規化
        heat_map = heat_map * (1. / max1)


        ####################################################################################################

        # for v in tf.global_variables():
        #   print(v)

        #        with tf.variable_scope("", reuse=True):
        #            var = tf.get_variable("conv1/kernel")
        #            print(var)
        # print(net.detection_variables)

        # モデル学習記録としてセッションを保存するsaveを用意
        saver = tf.train.Saver(max_to_keep=1000, var_list=net.detection_vars)

        # TensorBoardに学習記録を残す
     #   y_gray_images = tf.reshape(tf.transpose(y, perm=[0, 3, 1, 2]), [batch_size * num_classes, out_shape[0], out_shape[1], 1])
     #   y_s_gray_images = tf.reshape(tf.transpose(y_softmax, perm=[0, 3, 1, 2]), [batch_size * num_classes, out_shape[0], out_shape[1], 1])
     #   kernel3_images = tf.reshape(tf.transpose(kernel3, perm=[2, 3, 0, 1]), [-1, 3, 3, 1])

        # tensorboardに記録を残す
        with tf.name_scope('summary'):
            # tf.summary.scalar('loss_gt', loss_gt)
            # tf.summary.scalar('loss_fp', loss_fp)
             tf.summary.scalar('loss', loss)
             tf.summary.scalar('validation_loss', loss_valid)
             tf.summary.scalar('accuracy', accuracy)
             tf.summary.scalar('validation_accuracy', accuracy_valid)
            # tf.summary.image('y', y_gray_images, max_outputs=100)
            # tf.summary.image('y_softmax', y_s_gray_images, max_outputs=100)
    # #            tf.summary.image('kernel3', kernel3_images, max_outputs=100)
             #tf.summary.image('x', x, max_outputs=100)

    # #            tf.summary.image('y_softmax', y_softmax)
             merged = tf.summary.merge_all()

        with tf.Session(config=config) as sess:
            print("start session")

            # tf.Varableの初期化
            init = tf.global_variables_initializer()
            sess.run([init])
            sess.run(train_iter.initializer)
            sess.run(detect_iter.initializer)
            sess.run(train_iter_valid.initializer)

            print("init")
            #            pretrain_saver.restore(sess, "test/pretrain")
            #            saver.restore(sess, "main_train/test-40600")

            summary_writer = tf.summary.FileWriter(LOG_DIR, sess.graph)

            # 学習に使用される画像の可視化を行い確認
            test_img = sess.run(tf.convert_to_tensor(test_image))#detect_img)


            #            self.show(test_img, "testimg")
            #            cv2.waitKey(1)
            step = 0
            epoch = 0
            epoch2 = 0
            ####################################### (256,8, 8, 2)
            # Recognition Convolution-Netの学習を行う
            #######################################
            for _ in range(10000):
                try:

                    # モデルの評価を行う
                    loss_, accu, _ = sess.run([loss, accuracy, train_step])

                    loss_v, accu_v= sess.run([loss_valid, accuracy_valid])
                    n_ = sess.run(n)
                    y_ = sess.run(y)
                    print("n", np.shape(n_))
                    #print("y_", y_)
                    print("train: epoch={} epoch2={} accuracy={:02.6f} loss={}".format(epoch, epoch2,accu, loss_))
                    print("valid: epoch={} epoch2={} accuracy={:02.6f} loss={}".format(epoch, epoch2,accu_v, loss_v))
                    print("")

                  #  heat_map_net.set_weights(net.get_weights())

                    # 学習時ヒートマップ確認用
                    det = sess.run(heat_map, feed_dict={detect_input2: test_img*(1.0/256.0)})
                    det = det[0]
                    #test_img = sess.run(detect_img)
                   # det = det * (1. / np.amax(det))
                    resized_det = cv2.resize(det, (360, 360))
                    cv2.imshow("heatmap", resized_det)
                    #self.show(det, "heat_map")

                    resized = cv2.resize(cv2.vconcat(test_image), (360, 360))
                    cv2.imshow("input_img", resized)
                    cv2.waitKey(1)

                    summary = sess.run(merged)  # , feed_dict={x: x_, gt: gt_})
                    # gt: gt_,
                    # alpha:alpha_})

                    summary_writer.add_summary(summary, step)

                    epoch2+=1
                except tf.errors.OutOfRangeError as e:
                     epoch += 1
                     sess.run(train_iter.initializer)
                     sess.run(train_iter_valid.initializer)


                if step % 100 == 0:
                    #cnn_saver = tf.train.Saver(max_to_keep=1000, var_list=net.detection_vars)
                    #cnn_saver.restore(sess, "{}-{}".format(CNN_MODEL_SAVE_NAME, step))
                    if SAVE_MODEL:
                        print("save", step)
                        # 学習結果のセッションを保存
                        saver.save(sess, "test/main", global_step=step)

                step += 1
            summary_writer.close()
    # ↑️Recognition Model
    #################################################################################################################################
    # ↓Regression Model

    def make_noise(self, size):
        """
　　　　　Regression Modelを学習させるに当たり、ノイズを載せた正規分布画像を生成する(恐らく)
        :param size:
        :return:
        """
        ns_img = np.ndarray(size, dtype=np.float32)

        base = np.random.rand() * 1000

        for y in range(size[1]):
            for x in range(size[0]):
                ns_img[y, x] = (noise.pnoise2(x / size[0] * 5 + base, y / size[1] * 5, octaves=3) + 1.) / 2.

        return ns_img

    def norm_dist(self, size):
        """
        恐らく正規分布の擬似ヒートマップを生成する
        :param size:
        :return:
        """
        ns_img = np.ndarray(size, dtype=np.uint8)
        #        center = np.array(center)
        center = np.array([size[0] / 2, size[1] / 2])

        norm_zero = norm.pdf(x=0, loc=0, scale=WINDOW_SIZE / 2)
        for y in range(size[1]):
            for x in range(size[0]):
                p = np.array([x, y])
                d = np.linalg.norm(p - center)
                r = np.random.rand()

                ns_img[y, x] = norm.pdf(x=d, loc=0, scale=WINDOW_SIZE / 2) / norm_zero * 255

        return ns_img

    # paste_imgについて (http://opencv.jp/opencv-2svn/cpp/geometric_image_transformations.html)
    # void getRectSubPix(const Mat& image, Size patchSize, Point2f center, Mat& dst, int patchType=-1)
    # 画像から，矩形領域のピクセル値をサブピクセル精度で取得します．
    # パラメタ:
    # src – 入力画像
    # patchSize – 抽出される部分のサイズ
    # center – 入力画像から抽出される矩形の中心を表す，浮動小数点型の座標．この中心は，画像内に存在しなければいけません
    # dst – 抽出される部分．サイズは patchSize で， src と同じチャンネル数になります
    # patchType – 抽出されるピクセルのビット深度．デフォルトでは， src のビット深度と同じになります
    # 関数 getRectSubPix は， src からピクセルを抽出します：
    # dst(x, y) = src(x +  \texttt{center.x} - ( \texttt{dst.cols} -1)*0.5, y +  \texttt{center.y} - ( \texttt{dst.rows} -1)*0.5)
    def paste_img(self, src, center):
        """
        画像の内容は変更せずにピクセルグリッドだけを変形し，変形したグリッドを出力画像にマッピング
        :param src:
        :param center:
        :return:
        """
        return cv2.getRectSubPix(src, (INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE), center).astype(np.float) * (1. / 255)

    def run_regression(self):
        """
        Regression Modelを実行し、中心点を検出する
        :return:
        """
        # 学習データ元となる正規分布に従った画像をディレクトリから指定
        fname = "norm_dist{}x{}.png".format(INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE)

        # 正規分布のヒートマップ偽造画像を読み込む
        nd = cv2.imread(fname)
        # 擬似ヒートマップがディレクトリに用意されていない場合、生成する
        if nd is None:
            nd = self.norm_dist([INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE])
            cv2.imwrite(fname, nd)
        #        nd = np.delete(nd[:,:,0:1], [1,2], 2
        print(np.shape(nd))
        nd = nd[:, :, 0:1]
        # nd = nd.astype(np.float)*(1./255)
        print("擬似ヒートマップ-shape:{}".format(nd.shape))

        heatmap_size = REGRESSION_HEATMAP_SIZE  # grobal number 25
        batch_size = REGRESSION_BATCH_SIZE  # grobal number  64*16

        ################################
        # Regression Modelのインスタンス作成
        ################################
        net = ObjectDetectionNN()

        x = tf.placeholder(tf.float32, shape=[None, heatmap_size, heatmap_size, 1])
        t = tf.placeholder(tf.float32, shape=[None, 2])

        # Regression Modelの引数にPlaceholderをセット
        y = net.reg_network(x)

        # 損失を二乗和誤差で算出
        loss = tf.losses.mean_squared_error(t, y)
        # Adamで損失を最小化
        train_step = tf.train.AdamOptimizer(1e-4).minimize(loss)

        # 学習の状況を保存するSaverという機能がある.
        # この機能を使うことによって学習の一時停止を行ったり、学習済みの判別器を再利用可能.
        # http://discexuno.wp.xdomain.jp/machine-learning/use_tf_train_saver/
        saver = tf.train.Saver(max_to_keep=1000, var_list=net.regression_vars)

        with tf.Session(config=config) as sess:
            print("start session")
            # 変数初期化
            init = tf.global_variables_initializer()
            sess.run([init])

            # Regression Modelの学習を行う
            step = 0
            while True:

                # randomに画像及び中心点データ(教師データ)を生成
                images = np.ndarray(
                    [batch_size, heatmap_size, heatmap_size, 1], dtype=np.float32  # [64*16, 25, 25, 1]
                )
                xy = np.ndarray(
                    [batch_size, 2], dtype=np.float32
                )

                for i in range(batch_size):
                    # WINDOW_SIZE = 64 対象物体cropしている時のサイズっぽい恐らく　center=((32~192),(32~192))
                    center = (np.random.randint(round(WINDOW_SIZE / 2), round(INPUT_IMAGE_SIZE - WINDOW_SIZE / 2)),
                              np.random.randint(round(WINDOW_SIZE / 2), round(INPUT_IMAGE_SIZE - WINDOW_SIZE / 2)))

                    # 恐らく正規分布の形をランダムに変更している
                    size_fac = np.random.rand(2) * 0.8 + np.array([0.2, 0.2])
                    size = (np.array((WINDOW_SIZE * 4, WINDOW_SIZE * 4)) * size_fac).astype(np.int)
                    # print(size)
                    # 正規分布に従った擬似ヒートマップ
                    fake_obj = cv2.resize(nd, (size[0], size[1]))

                    # fake_objより短形領域をcropする　center=入力画像から抽出される矩形の中心
                    img = self.paste_img(fake_obj, center)
                    # ヒートマップのshapeにリサイズ
                    img = cv2.resize(img, (heatmap_size, heatmap_size))

                    # ここでノイズを混ぜている(足しているだけ)
                    images[i, :, :, 0] = img.astype(np.float32) + self.make_noise(img.shape)*np.random.rand()

                    # 正規化
                    xy[i, 0] = float(center[0]) / INPUT_IMAGE_SIZE  # INPUT_IMAGE_SIZE=256
                    xy[i, 1] = float(center[1]) / INPUT_IMAGE_SIZE
                    cv2.imwrite("./reg_data/"+str(i)+".jpg", img)

                # print(images.shape, images.dtype)
                # print(xy)
                # self.show_tile(images) 一旦外す
                # cv2.imshow("img",img)
                # cv2.waitKey(1)

                #########################################################
                # Regression Model学習部分　前処理したimages及びxyをモデルに流す
                #########################################################

                for _ in range(4):
                    _, loss_, y_ = sess.run([train_step, loss, y], feed_dict={x: images, t: xy})

                    print("loss: {}, {}".format(loss_, math.sqrt(loss_)))
                    print("teach " + str(xy[63]))
                    print("prediction " + str(y_[63]))

                    if step % 100 == 0:
                        if SAVE_MODEL:
                            print("save", step)
                            # セッションを保存する
                            saver.save(sess, "test/reg", global_step=step)

                    step += 1

    def run_test(self):
        """
        対象物体の中心座標を特定しopencvで可視化する関数　
        :return:
        """

        heatmap_size = REGRESSION_HEATMAP_SIZE

        ####################
        # 予測ネットワークの作成
        ####################
        net = ObjectDetectionNN()
        detect_img, _, detect_iter, _, _, _ = self.input_pipeline(filepath=DETECT_FILE,
                                                                  image_dir=DETECT_IMAGE_DIR,
                                                                  image_size=INPUT_IMAGE_PRE_RESIZE,  # 400 resize
                                                                  crop_size=INPUT_IMAGE_SIZE,       # 256
                                                                  batch_size=1,
                                                                  mode_train=False,
                                                                  validation_filepath=VALIDATION_FILE)

        ##################################
        # Recognition Convolution-Netの構築
        ##################################
        detect_input = tf.placeholder(tf.float32, [None, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, 3])
        cnn_out = net(detect_input)

        # softmax確認用
        out = net(detect_img, True)
        # バッチサイズを取得
#        batch_size = tf.shape(out)[0]

        # 無理矢理次元を落とし、Softmaxを算出っぽい
        #y = tf.reshape(out, [batch_size, 2])

        flat = tf.layers.flatten(out)
        dense = tf.layers.dense(flat, 2)

        # Recognition Modelの出力をsotfmax
        softmax = tf.nn.softmax(dense)

########################################################

        # Recognition Modelの出力を取得
        heat_map = cnn_out[:, :, :, 0:1]  # オブジェクトラベルを取得

        print("res:{}".format(heat_map))
        # 配列の中より最大値を取得することで出力値のヒートマップ化
        max1 = tf.reduce_max(heat_map, axis=None, keepdims=True)
        # 正規化
        heat_map = heat_map * (1. / max1)

        ###########################################################
        # Recognitionの出力を受けとるRegression Modelの構築(テスト時のみ)
        ###########################################################
        reg = net.reg_network(heat_map)

        # 学習済みセッションの呼び出し
        cnn_saver = tf.train.Saver(max_to_keep=1000, var_list=net.detection_vars)
        reg_saver = tf.train.Saver(max_to_keep=1000, var_list=net.regression_vars)

        for v in tf.global_variables():
            print(v)

        with tf.Session(config=config) as sess:
            print("start session")
            # 変数の初期化
            init = tf.global_variables_initializer()
            sess.run([init])
            sess.run(detect_iter.initializer)

            # Recognition Model 及び　Regression Modelの呼び出し
            #cnn_saver.restore(sess, "{}-{}".format(CNN_MODEL_SAVE_NAME, CNN_MODEL_READ_STEP))
            cnn_saver.restore(sess, "{}".format(CNN_MODEL_SAVE_NAME))
            reg_saver.restore(sess, "{}-{}".format(REG_MODEL_SAVE_NAME, REG_MODEL_READ_STEP))

         #   s = cnn_saver.get_tensor_by_name('VGG19test/my_convolution/conv4/heat_map:0')

            num = 0
            for _ in range(10000):

                test_img = sess.run(detect_img)
         #       s_ = sess.run([s], feed_dict={detect_input: test_img})
          #      print("s##############", s_)
                #            det = sess.run(heat_map, feed_dict={detect_input: test_img})
                # print(det.shape)
                # 分類CNN及び回帰CNNに画像を入力する
                det, pos = sess.run([heat_map, reg], feed_dict={detect_input: test_img})
                print(np.shape(det)) # (1, 25, 25, 1)
                print(det)

                print(pos[0][0])
                if math.isnan(pos[0][0]):
                    continue
                # det = det * (1. / np.amax(det))
                # print(det.shape,pos)

                det = det[0]
                # 分類CNNの出力を画像サイズにリサイズし拡大する
                det = cv2.resize(det, (INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE))  # 256x256

                # 中心座標情報
                mival, maval, miloc, maloc = cv2.minMaxLoc(det)
                # print(mival,maval, miloc, maloc)
                test_img = self.tensorToRGB(test_img[0])
                pos = pos[0]

                posy = int(pos[0] * test_img.shape[0])
                posx = int(pos[1] * test_img.shape[1])

                d = sess.run(softmax)

 #               print(float(d[:, 0]))
#                print(float(d[:, 1]))

                # 出力値が0.7より高い場合サークルを表示
               # if d[:, 0] < d[:, 1] and float(d[:, 1]) > 0.7:
                # サークル(中心座標)の表示
                cv2.circle(test_img, (posx, posy), 3, (0, 0, 255), thickness=3)  # 赤  regression model 予測
                # cv2.circle(test_img, maloc, 3, (255, 0, 255), thickness=3)  # ピンク ヒートマップ最大値
                font = cv2.FONT_HERSHEY_SIMPLEX
                cv2.putText(test_img, 'potC', (posx, posy), font, 0.6, (255, 0, 0), 2, cv2.LINE_AA)
                # cv2.putText(test_img, 'potC_max', maloc, font, 0.6, (255, 0, ), 2, cv2.LINE_AA)

                # テスト画像の表示
                cv2.imshow("input_img", test_img)
                #cv2.imwrite("./video/" + str(num)+"test.jpg", test_img)
                #                    out.write(test_img)
                print(num)
                print("MAX" + str(maloc))
                print("OUTPUT" + "(" + str(posx) + "," + str(posy) + ")")

                # center_point = input_image_size/2
                # RMSE_X_MAX = np.sqrt(np.mean((center_point-maloc[0])**2))
                # RMSE_Y_MAX = np.sqrt(np.mean((center_point-maloc[1])**2))
                # RMSE_X_OUT = np.sqrt(np.mean((center_point-posx)**2))
                # RMSE_Y_OUT = np.sqrt(np.mean((center_point-posy)**2))
                # RMSE_X = (RMSE_X_MAX + RMSE_X_OUT) /2
                # RMSE_Y = (RMSE_Y_MAX + RMSE_Y_OUT) /2
                # RMSE = (RMSE_X + RMSE_Y) /2
                # print("RMSE_X:"+str(RMSE_X)+"RMSE_Y:"+str(RMSE_Y)+"RMSE_XY:" + str(RMSE))

                num += 1
                # ヒートマップの表示
                cv2.imshow("heatmap", det)
                #cv2.imwrite("./video_det/" + str(num) + "det.jpg", det)
#                    out.write(det)
                cv2.waitKey(1)

