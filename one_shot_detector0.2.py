# -*- coding: utf-8 -*-

#using tensorflow 2.0rc

import tensorflow as tf

from tensorflow.python.layers import layers


import cv2
import numpy as np
import json
import sys
import time
import math
import glob
import os
import random
from enum import Enum
from scipy.stats import norm
import time
import noise
import matplotlib.pyplot as plt


save_model = True
read_model = False
model_path = ""


class Switch:

    def __init__(self, num):

        if num == 0:
            self.TRAIN_FILE = "/Users/matsunagamasaaki/MasterResearch/annotation/data/akamine_code/path_and_label_train.csv"
            self.VALID_FILE = "/Users/matsunagamasaaki/MasterResearch/annotation/data/akamine_code/path_and_label_valid.csv"
            self.TEST_FILE = '/Users/matsunagamasaaki/MasterResearch/annotation/data/akamine_code/path_and_label_test.csv'

        else:
            self.TRAIN_FILE = "/tmp/akamine_code/path_and_label_train.csv"  # #'train.csv'
            self.VALID_FILE = "/tmp/akamine_code/path_and_label_valid.csv"  # #'valid.csv'
            self.TEST_FILE = '/Users/matsunagamasaaki/MasterResearch/annotation/data/akamine_code/path_and_label_test.csv'


log_dir = 'logs'
REG_LOG_DIR = 'reg_logs'


TRAIN_IMAGE_PRE_RESIZE = None #or (128,128) for example. The input images are resized before cropping. Zero means to keep image size
OBJECT_IMAGE_SIZE = (64, 64) # The input images are cropped to this size
INPUT_IMAGE_CHANNELS = 3
BATCH_SIZE = 4*64
n_reader_threads = 8
#INPUT_IMAGE_SIZE = 400#256
INPUT_IMAGE_SIZE = 720
INPUT_IMAGE_SIZE2 = 405
CNN_MODEL_SAVE_NAME = "test2/main"
REG_MODEL_SAVE_NAME = "tmp/reg"

# どのepochのmodelを使うか
CNN_MODEL_READ_STEP = 400
REG_MODEL_READ_STEP = 1100


gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
config = tf.ConfigProto(device_count={'GPU': 0, 'CPU': 56}, gpu_options=gpu_options)


def ones(shape, name="none"):
    val = np.ones(shape, dtype=np.float32)*(1./(shape[0]*shape[1]*shape[2]))
    return tf.Variable(val, name=name)


class HeInitializer:
    def __init__(self):
        self.initializer = tf.variance_scaling_initializer()

    def __call__(self, shape, name):
        var = tf.Variable(self.initializer(shape=shape), name=name)
        return var


def low_value(shape, name):
    init = tf.constant(0.1, shape=shape)
    return tf.Variable(init, name=name)


class ObjectDetectionNN:
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.layers = []
        self.net = None
        self.regression_vars = []
        self.detection_vars = []

    def __call__(self, input, reuse=False):
        self.reuse = reuse
        return self.conv_network(input)

    def add(self, layer, *args):
        if self.net is None:
            self.net = layer
        else:
            self.net = layer(self.net)
          #  if int(args[0]) == 2:
          #      tf.identity(self.net, name="activation")

        if isinstance(layer, tf.keras.layers.Layer):
            self.layers.append(layer)

    def get_weights(self):
        weights = []
        for l in self.layers:
            weights.append(l.get_weights())

        return weights

    def set_weights(self, weights):
        for i in range(len(self.layers)):
            self.layers[i].set_weights(weights[i])

    def conv_network(self, images):
        activation=tf.nn.relu
        with tf.variable_scope("conv_layers"):
            #各レイヤーのチャンネルはもっと少なくても良いかもしれない
            self.add(images)
            self.add(tf.keras.layers.Conv2D(64,[5,5],[1,1]),"1")
            #self.add(tf.keras.layers.Activation(activation=tf.nn.relu),"2")
            #self.add(tf.identity(name="activation1"))
            self.add(tf.keras.layers.MaxPool2D([2,2],[2,2]),"3")
            self.add(tf.keras.layers.Conv2D(32,[3,3],[1,1]),"4")

            self.add(tf.keras.layers.MaxPool2D([2,2],[2,2]),"5")
            self.add(tf.keras.layers.Conv2D(32,[3,3],[1,1]),"6")
            self.add(tf.keras.layers.MaxPool2D([2,2],[2,2]),"7")
            self.add(tf.keras.layers.Conv2D(64, [6,6],[1,1]),"8") #これでちょうど[?,1,1,?]となるように，入力サイズを変えたらkernelを調整して[?,1,1,?]とする

            self.add(tf.keras.layers.Conv2D(64,[1,1],[1,1]),"9") #full connect

            self.add(tf.keras.layers.Conv2D(self.num_classes,[1,1],[1,1]),"10") #full connect

            self.detection_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='convnet')
            print(tf.shape(self.net))

        return self.net

    def conv2d(self, inputs, kernel_size, strides, filters, name, padding="VALID", activation=tf.nn.relu):
        """
        畳み込み層ーの実装が成されている
        :param inputs:
        :param kernel_size:
        :param strides:
        :param filters: Integer, the dimensionality of the output space (i.e. the number of filters in the convolution).
        :param name:
        :param padding:
        :param activation:
        :return:
        """
        layer = tf.layers.Conv2D(filters, kernel_size, strides, padding, activation=activation, _reuse=self.reuse,
                                 name=name)
        out = layer(inputs)

        return out

    def conv_network2(self, images):
        """
　　　　　転移学習あり
        VGG19の入力は4次元である必要がある
        :param images:
        :return:
        """
        reuse = self.reuse
        name = 'test'
        freeze = True

        # 初期設定はTrueになっている。変更するにはself.changer()を呼ぶことでFalseに変換
        with tf.variable_scope("VGG19" + name, reuse=reuse) as scope:

            if reuse:
                scope.reuse_variables()

            with tf.variable_scope("VGG19"):
                print("in:{}".format(images.shape))
                inputs = images  # tf.expand_dims(images[0], 0)
                print(inputs.shape)
                vgg19 = tf.keras.applications.VGG19(include_top=False,
                                                    weights='imagenet',
                                                    input_shape=(inputs.shape[1], inputs.shape[2], 3),  # (64, 64, 3),
                                                    input_tensor=inputs)
                if freeze:  # 下位層のみ学習する場合
                    for vgg_layer in vgg19.layers:
                        vgg_layer.trainable = True

                # vgg19の4層目にあたる出力を取得する
                vgg19_out = tf.identity(vgg19.layers[-20].output, name='output')
                print("name:{}".format(vgg19_out))
                print("transposed_shape:{}".format(vgg19_out.shape))

            activation = tf.nn.relu
            with tf.variable_scope("conv_layers"):
                # 各レイヤーのチャンネルはもっと少なくても良いかもしれない
                self.add(vgg19_out)
                self.add(tf.keras.layers.Conv2D(64, [5, 5], [1, 1]), "1")
                # self.add(tf.keras.layers.Activation(activation=tf.nn.relu),"2")
                # self.add(tf.identity(name="activation1"))
                self.add(tf.keras.layers.MaxPool2D([2, 2], [2, 2]), "3")
                self.add(tf.keras.layers.Conv2D(32, [3, 3], [1, 1]), "4")

                self.add(tf.keras.layers.MaxPool2D([2, 2], [2, 2]), "5")
                self.add(tf.keras.layers.Conv2D(32, [3, 3], [1, 1]), "6")
                self.add(tf.keras.layers.MaxPool2D([2, 2], [2, 2]), "7")
                self.add(tf.keras.layers.Conv2D(64, [6, 6], [1, 1]),
                         "8")  # これでちょうど[?,1,1,?]となるように，入力サイズを変えたらkernelを調整して[?,1,1,?]とする

                self.add(tf.keras.layers.Conv2D(64, [1, 1], [1, 1]), "9")  # full connect

                self.add(tf.keras.layers.Conv2D(self.num_classes, [1, 1], [1, 1]), "10")  # full connect

                self.detection_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope='convnet')
                print(tf.shape(self.net))

            return self.net

    def reg_network(self, heatmap):
        activation = tf.nn.relu
        with tf.variable_scope("regression"):
            net = tf.layers.conv2d(heatmap, 16, [3, 3], [1, 1], "SAME",
                                   activation=activation, name="reg_conv1")
            net = tf.layers.conv2d(net, 8, [3, 3], [1, 1], "SAME",
                                   activation=activation, name="reg_conv2")

            net = tf.layers.flatten(net)
            net = tf.layers.dense(net, 16, activation)
            net = tf.layers.dense(net, 16, activation)
            net = tf.layers.dense(net, 2, activation)
        # tf.get_collectionにより第二引数にscopeを指定することで、特定のscopeのものだけを取得する
        self.regression_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope='regression')
        return net


class NNPipeline:
    def __init__(self):
        pass

    def make_train_csv(self, train_image_csv, temp_path):

        classes = -1
        samples = []
        with open(train_image_csv) as f:
            for line in f:
                if line[0] == '#':
                    continue

                tokens = line.rstrip('\n').split(",")

                if tokens[0] == "classes":
                    classes = int(tokens[1])
                    continue

                if len(tokens) != 3:
                    raise ValueError("A line must have three items.")
                cat, num_copies, path = tokens

                path = path.replace("'", "")
                path = path.replace('"', "")
                num_copies = int(num_copies)

                for f in sorted(glob.glob(path)):
                    for _ in range(num_copies):
                        samples.append('{},{}'.format(cat, f))

        random.shuffle(samples)

        with open(temp_path,"w") as f:
            f.write("\n".join(samples))
        
        if classes > 1:
            return classes
        else:
            raise SyntaxError("A definition for number of classes is not found.")

    def input_pipeline(self, cvs_path, image_size, crop_size, batch_size,mode_train=True):
        def parse_line(line):
            (label, img_path) = tf.io.decode_csv(line, [[0.], ["no file"]])
            # tf.print([label,img_path])
            return (img_path, label)

        def read_image(image_path):
            # tf.print(image_path)
            jpeg_r = tf.io.read_file(image_path)
            image = tf.image.decode_jpeg(jpeg_r, channels=INPUT_IMAGE_CHANNELS)  # rgb
            return image

        def resize(image):
            if image_size is None:
                return image
            else:
                return tf.image.resize_images(image, image_size,
                                              method=tf.image.ResizeMethod.AREA,
                                              preserve_aspect_ratio=True)

        def random_crop(image):
            W = tf.minimum(crop_size[0], tf.shape(image)[0])
            H = tf.minimum(crop_size[1], tf.shape(image)[1])
            resized = tf.image.random_crop(image, [W, H, INPUT_IMAGE_CHANNELS])
            return resized

        def fit_size(image):
            resized = tf.image.resize_with_crop_or_pad(image,
                                                       crop_size[0], crop_size[1])
            return resized

        def normalize(image):
            return tf.cast(image, tf.float32) * (1. / 256)

        # if gt_flag == 0 else (resize(im), gt_flag)) \
        if mode_train:
            dataset = tf.data.TextLineDataset(cvs_path) \
                .skip(1) \
                .map(parse_line) \
                .map(lambda file, gt_flag: (read_image(file), gt_flag)) \
                .map(lambda im, gt_flag: (resize(im), gt_flag)) \
                .map(lambda im, gt_flag: (random_crop(im), gt_flag)) \
                .map(lambda im, gt_flag: (fit_size(im), gt_flag)) \
                .map(lambda im, gt_flag: (normalize(im), gt_flag))
        else:
            dataset = tf.data.TextLineDataset(cvs_path) \
                .skip(1) \
                .map(parse_line) \
                .map(lambda file, gt_flag: (read_image(file), gt_flag)) \
                .map(lambda im, gt_flag: (resize(im), gt_flag)) \
                .map(lambda im, gt_flag: (fit_size(im), gt_flag)) \
                .map(lambda im, gt_flag: (normalize(im), gt_flag))

        dataset = dataset.batch(batch_size=batch_size)
        dataset = dataset.prefetch(buffer_size=batch_size * 2)

        input_iter = dataset.make_initializable_iterator()
        images, flag = input_iter.get_next()
        #        crop = tf.image.crop_to_bounding_box(images, shape[1],shape[2],window_size,window_size)
        #        image = crop

        return images, tf.cast(flag, tf.int32), input_iter

    def run_epoch(self, sess, iter, tensor_list, by_step):
        """
        :param sess:
        :param iter:
        :param tensor_list:
        :param by_step: a function called by a step and returns [accuracy, batch size, summary]
        :return:
        """
        num_correct_samples = 0
        num_samples = 0
        while True:
            try:
                values = sess.run(tensor_list)

                accu, num, summary = by_step(values)
                num_correct_samples += accu * num
                num_samples += num

            except tf.errors.OutOfRangeError as e:
                sess.run(iter.initializer)
                break

        return (num_correct_samples / num_samples, summary)

    def make_input_graph(self, train_image_csv):
        temp_csv = train_image_csv+".extracted.csv"
        num_classes = self.make_train_csv(train_image_csv, temp_csv)

        img, label, train_iter = self.input_pipeline(cvs_path=temp_csv,
                                                     image_size=TRAIN_IMAGE_PRE_RESIZE,
                                                     crop_size=OBJECT_IMAGE_SIZE,
                                                     batch_size=BATCH_SIZE,
                                                     mode_train=True,
                                                     )

        return img, label, train_iter, num_classes

    def make_test_input_graph(self, test_image_csv):

        temp_csv = test_image_csv+".extracted.csv"
        num_classes = self.make_train_csv(test_image_csv,  temp_csv)

        img, label, test_iter = self.input_pipeline(cvs_path=temp_csv,
                                                    image_size=(400,400),
                                                    crop_size=(256,256),
                                                    batch_size=1,
                                                    mode_train=False
                                                    )
        return img, label, test_iter, num_classes

    def make_recog_graph(self, input, label, num_output):
        net = ObjectDetectionNN(num_output)

        x = input
        t = tf.one_hot(label, num_output)
        y = net(x)

        batch_size = tf.shape(y)[0]
        y = tf.reshape(y, [batch_size, num_output])

        softmax = tf.nn.softmax(y)
        loss = tf.losses.softmax_cross_entropy(t, softmax)

        train_step = tf.train.AdamOptimizer(1e-4).minimize(loss)

        correct_prediction = tf.equal(tf.argmax(y, 1), tf.cast(label, tf.int64))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        
        # get accuracy for samples belonging to class x
        class_x = 0
        correct_prediction_for_x = tf.gather_nd(correct_prediction, tf.where(tf.equal(label, class_x)))
        accuracy_for_x = tf.reduce_mean(tf.cast(correct_prediction_for_x, tf.float32))

        return train_step, loss, accuracy, accuracy_for_x, y, net

    def make_recog_graph_for_test(self):

        num_output = 2
        net = ObjectDetectionNN(num_output)

        test_image_ = tf.placeholder(tf.float32, shape=[None, 64, 64, 3], name="calculate_accuracy")
        y = net(test_image_* (1.0/255))

        #batch_size = tf.shape(y)[0]
        #y = tf.reshape(y, [batch_size, num_output])

        softmax = tf.nn.softmax(y)

        return test_image_, softmax, y

    def make_heatmap_graph(self, num_output):
        net = ObjectDetectionNN(num_output)

        test_image = tf.placeholder(tf.float32, shape=[None, 405, 720, 3])
        y = net(test_image * (1.0/255))

        yshape = tf.shape(y)
        y = tf.reshape(y, [yshape[0]*yshape[1]*yshape[2], num_output])
        y = tf.nn.softmax(y)
        y = tf.reshape(y, yshape)
        print("y_shape",np.shape(y))

        return test_image, y, net

    def make_heatmap_graph_for_reg(self, num_output):
        net = ObjectDetectionNN(num_output)

        test_image = tf.placeholder(tf.float32, shape=[None, 400, 400, 3])
        y = net(test_image * (1.0 / 255))

        yshape = tf.shape(y)
        y = tf.reshape(y, [yshape[0] * yshape[1] * yshape[2], num_output])
        y = tf.nn.softmax(y)
        y = tf.reshape(y, yshape)
        print("y_shape", np.shape(y))

        return test_image, y, net

    def train(self, train_image_csv, valid_image_csv):

        img, label, train_iter, num_classes = self.make_input_graph(train_image_csv)
        train_step, loss, accuracy, accuracy_for_x, y, train_net = self.make_recog_graph(input=img, label=label, num_output=num_classes)

        tf.summary.scalar("loss", loss)
        tf.summary.scalar("accuracy", accuracy)

        vimg, vlabel, val_train_iter, num_classes = self.make_input_graph(valid_image_csv)
        _, _, val_accuracy, val_accuracy_for_x, val_y, valid_net = self.make_recog_graph(input=vimg, label=vlabel, num_output=num_classes)

        test_image_input, heatmap, heatmap_net = self.make_heatmap_graph(num_classes)

        test_image_input_reg,heatmap_reg,heatmap_net_reg = self.make_heatmap_graph_for_reg(num_classes)

        merged = tf.summary.merge_all()

        conv_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope="VGG19test")#"conv_layers")
        saver = tf.train.Saver(max_to_keep=1000, var_list=conv_vars)


        detect_input = tf.placeholder(tf.float32, [None, 405, 720, 3])#[None,260,260,3])#[None, 405, 720, 3])

        reg = heatmap_net.reg_network(heatmap[:,:,:,0:1])#detect_input)
        reg_saver = tf.train.Saver(max_to_keep=1000, var_list=heatmap_net.regression_vars)

        # 画像を１枚指定し、訓練中正しく学習が進行しているか確認するための画像
        test_image_path = [#"/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/587.jpg",
                           "/Users/matsunagamasaaki/MasterResearch/annotation/data/IN/1077.jpg"]
                           #"/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/428.jpg",
                           #"/Users/masaaki/akamineresearch/Master_Research/annotation/data/IN/1406.jpg"]

        test_image = 0
        test_images = []
        for path in test_image_path:
            test_image = cv2.imread(path)
            #test_image = cv2.resize(test_image, (260, 260))

            test_images.append(test_image)
            #画像の
            test_image = np.stack(test_images)


        test_images_reg=[]
        # ヒートマップ獲得用
        with open("/Users/matsunagamasaaki/MasterResearch/annotation_for_reg/akamine_code/path_and_label_test_reg.csv", "r") as f:
            line = f.readlines()

        lines=[]
        for i in line:
            i=i.strip('\n')
            lines.append(i)
        print(lines)



        with tf.Session(config=config) as sess:
            intermediate_output=0
            a=sess.graph.get_operations()
            for i in a:
            #    print(i.name)
                if i.name == "conv_layers/conv2d_1/Conv2D":#"conv_layers/conv2/Conv2D":
                    intermediate_output = i.values()

            print("start session")
            init = tf.global_variables_initializer()
            sess.run([init])
            sess.run(train_iter.initializer)
            sess.run(val_train_iter.initializer)
            #sess.run(detect_iter.initializer)
            REG_MODEL_SAVE_NAME = "test/reg"
            REG_MODEL_READ_STEP = 200
          #  reg_saver.restore(sess, "{}-{}".format(REG_MODEL_SAVE_NAME, REG_MODEL_READ_STEP))
            print("init")

            summary_writer = tf.summary.FileWriter(log_dir, sess.graph)


            reg_count = 0
            for epoch in range(500000):

                l = sess.run(label)
              #  print("++++++",l)
              #  dd = sess.run(img)
              #  print(np.shape(dd))

                count=0
                for i in l:

                    if i == 1:
                        break
                    count+=1

                #if i ==1:
                te = Tester()
               # te.show_intermediate_heatmap(sess,intermediate_output,test_image_input,img)


                """
                imgg = sess.run(intermediate_output)
                # print("+++++++++++++++++++++++",np.shape(imgg))

                row = np.array(imgg)[0, count, :, :, 0:1]

                # intermediate_activation_map = np.array(imgg)[0, 0, :, :, :]

                # print("original", np.shape(row))
                row_ = []
                final = 0
                count = 0
                intermediate_activation_map = 0
                for h in range(1, 32):
                    # activationをmap取得
                    imgg_ = np.array(imgg)[0, 0, :, :, h:h + 1]
                    intermediate_activation_map += imgg_

                    row = np.hstack((row, imgg_))
                    # print("row", np.shape(row))
                    if h % 4 == 0:
                        row_.append(row)
                        row = imgg_
                        if count == 0:
                            final = row_[0]
                        if count > 0:
                            final = np.vstack((final, row_[count]))
                        #        print("final", np.shape(final))
                        count += 1

                intermediate_activation_map = intermediate_activation_map / 32
                # print("intermediate;",np.shape(intermediate_activation_map))
                intermediate_activation_map = cv2.resize(intermediate_activation_map, (360, 280))
                print("intermediate;", np.shape(intermediate_activation_map))
                cv2.imshow("intermediate_conv2_mean_map", intermediate_activation_map * 255)
                
                
                # print("final",np.shape(final))

                resized_imgg_ = cv2.resize(final, (470, 700))  # (470,1176))#(672,168))
                cv2.imshow("intermediate_conv2_activations", resized_imgg_)
                """
                def by_step(values):
                    summary, loss, accuracy, accuracy_for_x, y, _, img = values
                    print("  step accuracy={:02.6f} {:02.6f} loss={}".format(accuracy, accuracy_for_x, loss))
                    #Tester.show_tile(img)
                    #cv2.waitKey(1)
                    return accuracy, y.shape[0], summary

                epoch_accu, summary_ = self.run_epoch(sess, train_iter, [merged, loss, accuracy, accuracy_for_x, y, train_step, img], by_step)
                print("train epoch={} accuracy={:02.6f} ".format(epoch, epoch_accu))

                def by_step_val(values):
                    accuracy, accuracy_for_x, y = values
                    print("  val step accuracy={:02.6f} {:02.6f}".format(accuracy, accuracy_for_x))
                    return accuracy, y.shape[0], None

         #       valid_net.set_weights(train_net.get_weights()) #share weights with training network
                epoch_accu, _ = self.run_epoch(sess, val_train_iter, [val_accuracy, val_accuracy_for_x, val_y], by_step_val)
                print("valid epoch={} accuracy={:02.6f} ".format(epoch, epoch_accu))

                heatmap_net.set_weights(train_net.get_weights()) #share weights with training network
                heatmap_net_reg.set_weights(train_net.get_weights())

                heatmap_ = sess.run(heatmap, feed_dict={test_image_input: test_image})

                try:
                    img1=lines[reg_count]
                    reg_count+=1
                except IndexError:
                    reg_count=0
                    img1 = lines[reg_count]
                
                image = cv2.imread(img1)
                
                resized = cv2.resize(image, (400, 400))
                resized_for_check = cv2.resize(image, (360, 280))
                cv2.imshow('for_reg', resized_for_check)
                resised_ = [resized]
                test_image_for_reg = np.stack(resised_)

                heatmap_for_reg = sess.run(heatmap_reg, feed_dict={test_image_input_reg: test_image_for_reg})

                pos = sess.run(reg, feed_dict={test_image_input: test_image})
                print("heatmap shape", np.shape(heatmap_))

                test = Tester()
                test.show(heatmap_, pos, test_image, show_max=1)
                test.show(heatmap_for_reg, pos, test_image_for_reg, show_max=1)
                cv2.waitKey(1)


                #intermediate_layer_output = sess.run(sess.graph.get_tensor_by_name('activation:0'),
                #                                     feed_dict={test_image_input: test_image})

                if epoch % 10 == 0:
                    if save_model:
                        print("save", epoch)
                        saver.save(sess, "tmp/main") #, global_step=step

                    summary_writer.add_summary(summary_, epoch)

            summary_writer.close()


    def make_noise(self, size):
        """
　　　　　Regression Modelを学習させるに当たり、ノイズを載せた正規分布画像を生成する(恐らく)
        :param size:
        :return:
        """
        ns_img = np.ndarray(size, dtype=np.float32)

        base = np.random.rand() * 1000

        for y in range(size[1]):
            for x in range(size[0]):
                ns_img[x,y] = (noise.pnoise2(x/ size[1] * 5 + base, y / size[0] * 5, octaves=3)+1.) / 2.

        return ns_img

    def norm_dist(self, size):
        """
        恐らく正規分布の擬似ヒートマップを生成する
        :param size:
        :return:
        """
        WINDOW_SIZE=64
        ns_img = np.ndarray(size, dtype=np.uint8)
        #        center = np.array(center)
        center = np.array([size[0] / 2, size[1] / 2])

        norm_zero = norm.pdf(x=0, loc=0, scale=WINDOW_SIZE / 2)
        for y in range(1,size[1]):
            for x in range(1,size[0]):
                p = np.array([x, y])
                d = np.linalg.norm(p - center)
                r = np.random.rand()

                ns_img[x, y] = norm.pdf(x=d, loc=0, scale=WINDOW_SIZE / 2) / norm_zero * 255

        return ns_img

        # paste_imgについて (http://opencv.jp/opencv-2svn/cpp/geometric_image_transformations.html)
        # void getRectSubPix(const Mat& image, Size patchSize, Point2f center, Mat& dst, int patchType=-1)
        # 画像から，矩形領域のピクセル値をサブピクセル精度で取得します．
        # パラメタ:
        # src – 入力画像
        # patchSize – 抽出される部分のサイズ
        # center – 入力画像から抽出される矩形の中心を表す，浮動小数点型の座標．この中心は，画像内に存在しなければいけません
        # dst – 抽出される部分．サイズは patchSize で， src と同じチャンネル数になります
        # patchType – 抽出されるピクセルのビット深度．デフォルトでは， src のビット深度と同じになります
        # 関数 getRectSubPix は， src からピクセルを抽出します：
        # dst(x, y) = src(x +  \texttt{center.x} - ( \texttt{dst.cols} -1)*0.5, y +  \texttt{center.y} - ( \texttt{dst.rows} -1)*0.5)

    def paste_img(self, src, patchsize_x, patchsize_y, center):
        """
        srcimageから指定領域をトリミング
        :param src:
        :param center:
        :return:
        """
        return cv2.getRectSubPix(src, (patchsize_x, patchsize_y), center).astype(np.float) * (1. / 255)

    def run_regression(self):
        """
        Regression Modelを実行し、中心点を検出する
        :return:
        """


        # 学習データ元となる正規分布に従った画像をディレクトリから指定
        fname = "norm_dist{}x{}.png".format(256, 256)

        # 正規分布のヒートマップ偽造画像を読み込む
        nd = cv2.imread(fname)
        # 擬似ヒートマップがディレクトリに用意されていない場合、生成する
        if nd is None:
            nd = self.norm_dist([64, 64])
            cv2.imwrite(fname, nd)
        #        nd = np.delete(nd[:,:,0:1], [1,2], 2

        nd = nd[:, :, 0:1]
        # nd = nd.astype(np.float)*(1./255)
        print("擬似ヒートマップ-shape:{}".format(nd.shape))

        heatmap_size = 43 # grobal number 25 43
        heatmap_size2 = 83 # 83
        batch_size = 64*16  # grobal number  64*16

        ################################
        # Regression Modelのインスタンス作成
        ################################
        net = ObjectDetectionNN(2)
        x = tf.placeholder(tf.float32, shape=[None, heatmap_size, heatmap_size2, 1], name='pseudo_train_image')
        t = tf.placeholder(tf.float32, shape=[None, 2], name='center_label')

        # Regression Modelの引数にPlaceholderをセット
        y = net.reg_network(x)

        # 損失を二乗和誤差で算出
        loss = tf.losses.mean_squared_error(t, y)

        # Adamで損失を最小化
        train_step = tf.train.AdamOptimizer(1e-4).minimize(loss)

        accuracy = tf.reduce_mean(tf.abs(t-y))

        # 学習の状況を保存するSaverという機能がある.
        # この機能を使うことによって学習の一時停止を行ったり、学習済みの判別器を再利用可能.
        # http://discexuno.wp.xdomain.jp/machine-learning/use_tf_train_saver/
        saver = tf.train.Saver(max_to_keep=1000, var_list=net.regression_vars)

        with tf.name_scope('summary'):
            tf.summary.scalar('loss', loss)
            tf.summary.scalar('accuracy(MeanError)', accuracy)
            merged = tf.summary.merge_all(name='regression')

        with tf.Session(config=config) as sess:
            print("start session")


            # 変数初期化
            init = tf.global_variables_initializer()

            sess.run([init])

            summary_writer = tf.summary.FileWriter(REG_LOG_DIR, sess.graph)
            # Regression Modelの学習を行う
            step = 0
            with open('/Users/matsunagamasaaki/MasterResearch/annotation_for_reg/akamine_code/path_and_label_test_reg_train.csv','r') as f:
            #with open(  '/tmp/annotation_for_reg/akamine_code/path_and_label_test_reg_train.csv',  'r') as f:
                line=f.readlines()

            lines = []
            for i in line:
                i = i.strip('\n')
                lines.append(i)

            while True:
                # randomにヒートマップ画像及び中心点データ(教師データ)を生成(中身は0)\
                # ここはチェックしたところ正しい
                images = np.ndarray(
                    [batch_size, heatmap_size, heatmap_size2, 1], dtype=np.float32  # [64*16, 25, 25, 1]
                )
                xy = np.ndarray(
                    [batch_size, 2], dtype=np.float32
                )

                WINDOW_SIZE=64
                index =0
                for i in range(batch_size):

                    # ラベル(中心座標)の自動生成
                    # 256の入力画像に対し WINDOW_SIZE = 64の擬似ヒートマップを移動。　center=((32~224),(32~224))
                    center = (np.random.randint(round(WINDOW_SIZE / 2), round(256 - WINDOW_SIZE / 2)),
                              np.random.randint(round(WINDOW_SIZE / 2), round(256 - WINDOW_SIZE / 2)))
                    # 乱数生成
                    size_fac = np.random.rand(2) * 0.8 + np.array([0.2, 0.2])
                    size = (np.array((WINDOW_SIZE * 4, WINDOW_SIZE * 4)) * size_fac).astype(np.int)

                    # print(size)
                    try:
                        path = lines[index]
                        index += 1
                    except IndexError:
                        index = 0
                        path = lines[index]

                    potc = cv2.imread(path, flags=0)#'norm_dist_real.jpg'

                    # オリジナル擬似ヒートマップの形を変形
                    fake_obj = cv2.resize(potc, (size[0], 3*size[0]))#size[1])) #potcはもともとnd

            #        cv2.imwrite('./im/test'+str(i)+'.jpg', fake_obj)
                 #   print(np.shape(fake_obj))

                    ##################################################################
                    # fake_objより短形領域をcropする　center=入力画像から抽出される矩形の中心
                    ##################################################################
                    img = self.paste_img(potc.copy(), 720, 405, center)

               #     print('--------------------------------------', center)
#                    cv2.circle(img, (center[0], center[1]), 3,
#                               (0, 0, 0), thickness=2)
#                    cv2.putText(img, "*", (int(size[0]/2), int(size[0]/2)),
#                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 255), 2, cv2.LINE_AA)

            #        cv2.imwrite("./im/put"+str(i)+".jpg", img*256)


                    # ヒートマップのshapeにリサイズ(ここがヒートマップ)
                    img = cv2.resize(img, (heatmap_size2, heatmap_size))
             #       cv2.imwrite("./im/post"+str(i)+".jpg", img*255)


                    # ここでノイズを混ぜている(足しているだけ)代入
                    images[i, :, :, 0] = img.copy().astype(np.float32)+ self.make_noise(img.shape)#* np.random.rand()
                     #サイズ25の空の配列に                # 25の擬似ヒートをキャストして移植

                    #################
                    #アレンジ
                   # print(np.shape(images[i]))

                    # Opencvの関数に依存して、ヒートマップの位置を検出してるっぽい(類似度の最大値)(テンプレートマッチングは行ってない)
                    point = cv2.minMaxLoc(images[i][:, :, 0:1])



                    w,h,z= np.shape(images[i][:, :, :])
                    w=10
                    h=10


                    bottom_right = [point[3][0]+w, point[3][1] +h]



                    real_center = [bottom_right[0] - point[3][0], abs(point[3][1]- bottom_right[1])]


                    ################r
                    # 多分中心点 .. 正規化している?
                    xy[i, 0] = float(point[3][0]/43)# float(center[0]) / 360#INPUT_IMAGE_SIZE2  # INPUT_IMAGE_SIZE=256
                    xy[i, 1] = float(point[3][1]/83)#float(center[1]) / 360#INPUT_IMAGE_SIZE
                    resize = cv2.resize(img, (360, 202))
               #     cv2.imwrite("./reg_data/"+str(i)+".jpg", resize*256)

                    cv2.imwrite('./im/'+str(i)+'.jpg', images[i, :, :, 0]*255)

                    import csv
                    with open('check.csv', 'a') as f:
                        writer = csv.writer(f)
                        writer.writerow(['./im/'+str(i)+'.jpg', [xy[i,0]*43,xy[i,1]*83]])

                    import pandas as pd
                    df = pd.DataFrame(xy)
               #     df.to_csv('./validate_regression/validate.csv')


                   # print(xy)
                    tester = Tester()

#                    tester.show_tile(images) #一旦外す
#a                    img = cv2.resize(img, (360, 280))
#                    cv2.imshow("img", img)
#                    cv2.waitKey(1)

                #########################################################
                # Regression Model学習部分　前処理したimages及びxyをモデルに流す
                #########################################################
                epoch_counter=0
                for i in range(20):

                    _, loss_, y_, accu = sess.run([train_step, loss, y, accuracy], feed_dict={x: images, t: xy})
                    print('STEP: ', str(i), 'EPOCH: ', str(epoch_counter))
                    print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', np.shape(images)),np.shape(xy)

                    #accu = sess.run(accuracy, feed_dict={x: images, t: xy})
                    print(np.shape(xy))
                    print("loss: {}, {}".format(loss_, math.sqrt(loss_)))
                    print("label " + str(xy[63]))
                    print("predi " + str(y_[63]))
                    print("accuracy(Mean Error): {}".format(accu)) # 平均誤差
                    print(np.shape(images))
                    print(images.shape[0])
                    print(xy[63][0])

                    # 中心座標回帰確認用
                    resized = cv2.resize(images[63], (83, 43))
                    test = Tester()
                    a = test.tensorToRGB(images[63][:, :, 0:1])
                    print('ssssssssssssss', a.shape[1])

                   # cv2.circle(resized, ((xy[63][0] * 43).astype(np.int),(xy[63][1]*83).astype(np.int)), 3, (0, 0, 0), thickness=3)  # ラベル
                    cv2.circle(resized, ((y_[63][0] * 43).astype(np.int),(y_[63][1]*83).astype(np.int)), 3, (255, 255, 255), thickness=3)  # 予測
                    cv2.putText(resized, "*", ( (xy[63][0] * 43).astype(np.int64),(xy[63][1]*83).astype(np.int64)),  cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 255), 2, cv2.LINE_AA)
                #    cv2.imshow('img', resized)
                #    cv2.waitKey(1)

                #    if i == 19:
                 #       cv2.destroyAllWindows()

                    summary = sess.run(merged, feed_dict={x: images, t: xy})
                    summary_writer.add_summary(summary, step)

                    if step % 100 == 0:
                        if True:
                            print("================save================", step)
                            # セッションを保存する
                            saver.save(sess, "tmp/reg", global_step=step)
                            epoch_counter+=1

                    step += 1

                summary_writer.close()

    def run_test(self, test_image_csv, r_for_pointing, r):
        """
        対象物体の中心座標を特定しopencvで可視化する関数　
        :return:
        """

        import pandas as pd

        TEST_FILE2 = '/Users/matsunagamasaaki/MasterResearch/annotation/data/test_data2.csv'#'/tmp/annotation/data/test_data.csv'#

        #'/Users/matsunagamasaaki/MasterResearch/annotation/data/akamine_code/path_and_label_test2.csv'
        # "/Users/matsunagamasaaki/MasterResearch/annotation_for_reg/akamine_code/path_and_label_test_reg.csv"#
        test_data_series =pd.read_csv(TEST_FILE2)
        df = pd.DataFrame(test_data_series)
        print('ss', df['center'].as_matrix()[0])

        # center-point-detectorヒートマップ画像
        locate = '/Users/matsunagamasaaki/MasterResearch/annotation/data/locate.csv'
        test_data_series = pd.read_csv(locate)
        df_ = pd.DataFrame(test_data_series)
        # center-point-detectorオリジナル画像
        original = '/Users/matsunagamasaaki/MasterResearch/annotation/data/original.csv'
        test_data_series = pd.read_csv(original)
        df_original = pd.DataFrame(test_data_series)

        """
        # centerのラベル情報が無い場合におけるテストデータを生成コード
        test_image_path = []
        with open(TEST_FILE2, 'r') as f:
            line = f.readlines()

        for i in line:
            d = i.strip("\n")
            print(d)
            test_image_path.append(str(d))
        """

        test_image_path = df['path'].as_matrix()



        test_image_input, heatmap, heatmap_net = self.make_heatmap_graph(2)

        reg = heatmap_net.reg_network(heatmap[:, :, :,1:2])

        # 重みの呼び出し
        conv_vars = tf.get_collection(tf.GraphKeys.VARIABLES, scope="conv_layers")
        cnn_saver = tf.train.Saver(max_to_keep=1000, var_list=conv_vars)
        reg_saver = tf.train.Saver(max_to_keep=1000, var_list=heatmap_net.regression_vars)

        img, accuracy, before_softmax = self.make_recog_graph_for_test()
        with tf.Session() as sess:
            print("start session")
            # 変数の初期化
            init = tf.global_variables_initializer()
            sess.run([init])

            layers = sess.graph.get_operations()
            for layer in layers:
           #     print(layer.name)
                if layer.name == "conv_layers/conv2d_4/Conv2D":
                    intermediate_output = layer.values()

            # Recognition Model 及び　Regression Modelの呼び出し
            cnn_saver.restore(sess, "{}".format(CNN_MODEL_SAVE_NAME))
            reg_saver.restore(sess, "{}-{}".format(REG_MODEL_SAVE_NAME, REG_MODEL_READ_STEP))

            #   s = cnn_saver.get_tensor_by_name('VGG19test/my_convolution/conv4/heat_map:0')

            """
            # for training regression cnn
            test_image_path = ["/Users/matsunagamasaaki/MasterResearch/annotation/data/frames_in/0/5/502.jpg"]

            test_image2 = 0
            test_images2 = []
            for path in test_image_path:
                test_image = cv2.imread(path)
                # test_image = cv2.resize(test_image, (260, 260))

                test_images2.append(test_image)
                # 画像の
                test_image2 = np.stack(test_images2)

            h = sess.run(heatmap, feed_dict={test_image_input: test_image2})
            h = h[0]
            resized_imgg_ = cv2.resize(h, (360, 280))  # (470,1176))#(672,168))
            print(np.shape(resized_imgg_[:,:,0:1]))

            cv2.imwrite("tmp.jpg", resized_imgg_[:, :, 1:2]*255)
            """
            num = 0
            tp = 0
            tn = 0
            fp = 0
            fn = 0
            tp_max = 0
            tn_max = 0
            fp_max = 0
            fn_max = 0

            locate_image_path = df_['path'].as_matrix()
            original_image_path = df_original['path'].as_matrix()
            print(locate_image_path)
            t=0
            for i, center_label in zip(test_image_path, df['center'].as_matrix()):#range(10000):
                t+=1
                print(t)
                locate = 0
                locate_images = []
                try:
                    a=locate_image_path[t]
                except TypeError:
                    break

                for path in [a]:
                    locate_ = cv2.imread(path)
                    print(locate_)
                    print(np.shape(locate_))
                    #locate_image = cv2.resize(locate_, (720, 405))
                    #locate_images.append(locate_image)
                    # 画像の
                    pass
                #    locate = np.stack(locate_images)


                try:
                    b=original_image_path[t]
                except TypeError:
                    break

                for path in [b]:
                    original_image = cv2.imread(path)
                    # 画像の

                center_label=center_label.split(',')
                center_label=[round(float(i.strip('[').strip(']'))) for i in center_label]



                #test_img = sess.run(detect_img)
                #       s_ = sess.run([s], feed_dict={detect_input: test_img})
                #      print("s##############", s_)
                #            det = sess.run(heat_map, feed_dict={detect_input: test_img})
                # print(det.shape)
                # 分類CNN及び回帰CNNに画像を入力する

                test_image_path = [i]

                test_image = 0
                test_images = []
                for path in test_image_path:
                    test_image = cv2.imread(path)
                    test_image = cv2.resize(test_image, (720, 405))
                    test_image_for_accuracy = cv2.resize(test_image, (64, 64)) # for calculate accuracy
                    test_images.append(test_image)
                    # 画像の
                    test_image = np.stack(test_images)

                # 画像を縮小してaccuracyを算出
                accu = sess.run(accuracy, feed_dict={img: np.expand_dims(test_image_for_accuracy, axis=0)})
    #            print("accuracy={} ".format(accu))

                det_, pos = sess.run([heatmap, reg], feed_dict={test_image_input: test_image})

                if math.isnan(pos[0][0]):

                    continue


                """
            #    from Modules import utils

                prob = sess.run(accuracy[:, 0, 0, :], feed_dict={images: test_image})

                gb_grad_value, target_conv_layer_value, target_conv_layer_grad_value = sess.run(
                    [gb_grad, target_conv_layer, target_conv_layer_grad],
                    feed_dict={images: test_image, labels: 1})

                for i in range(BATCH_SIZE):
                    utils.print_prob(prob[i], './synset.txt')
                    # VGG16 use BGR internally, so we manually change BGR to RGB
                    gradBGR = gb_grad_value[i]
                    gradRGB = np.dstack((
                        gradBGR[:, :, 2],
                        gradBGR[:, :, 1],
                        gradBGR[:, :, 0],
                    ))

                    utils.visualize(test_image[i], target_conv_layer_value[i], target_conv_layer_grad_value[i], gradRGB)
                """




                #pos = sess.run(reg, feed_dict={test_image_input: test_image})
         #       s = sess.run(score, feed_dict={test_image_input: test_image})

                """
                test = Tester()
                test2 = cv2.resize(test_image[0], (360, 280))
                cv2.imshow("input_img", test2)
                test.show(det, pos, test_image, show_max=1)
                cv2.waitKey(1)
                """
                #print("test",np.shape(test_image))
                # visualize intermediate layer output

                """
                # 中間層の可視化
                imgg = sess.run(intermediate_output,feed_dict={test_image_input:test_image})
                #print("+++++++++++++++++++++++",np.shape(imgg))
    
                row = np.array(imgg)[0, 0, :, :, 0:1]
                #intermediate_activation_map = np.array(imgg)[0, 0, :, :, :]
    
                #print("original", np.shape(row))
                row_ = []
                final = 0
                count = 0
                intermediate_activation_map=0
                for h in range(1, 32):
                    # activationをmap取得
                    imgg_ = np.array(imgg)[0, 0, :, :, h:h + 1]
                    intermediate_activation_map += imgg_
    
                    row = np.hstack((row, imgg_))
                   # print("row", np.shape(row))
                    if h % 4 == 0:
                        row_.append(row)
                        row = imgg_
                        if count == 0:
                            final = row_[0]
                        if count > 0:
                            final = np.vstack((final, row_[count]))
                    #        print("final", np.shape(final))
                        count += 1
                intermediate_activation_map = intermediate_activation_map/32
                #print("intermediate;",np.shape(intermediate_activation_map))
                intermediate_activation_map = cv2.resize(intermediate_activation_map,(360, 280))
                print("intermediate;", np.shape(intermediate_activation_map))
    #                cv2.imshow("intermediate_conv2_mean_map", intermediate_activation_map*255)
    
                # print("final",np.shape(final))
    
                resized_imgg_ = cv2.resize(final, (470, 700))  # (470,1176))#(672,168))
    #                cv2.imshow("intermediate_conv2_activations", resized_imgg_)
                """



                det = det_[0]
           #     print("score", s)
                # 分類CNNの出力を画像サイズにリサイズし拡大する
                det = cv2.resize(det, (576, 324))# (INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE))  # 256x256

                # 中心座標情報
                mival, maval, miloc, maloc = cv2.minMaxLoc(det[:, :, 1:2])
                # print(mival,maval, miloc, maloc)
                test_image1 = cv2.resize(test_image[0], (720, 405))#test_image[0]#cv2.resize(test_image[0], (360,280))

                test = Tester()
                a = test.tensorToRGB(test_image1[:,:,1:2])

                pos = pos[0]
                print(pos)
                print(a.shape)
                posx = int(pos[1]* a.shape[1]) # 83
                posy = int(pos[0]* a.shape[0]) # 43



                # center label
                #cv2.drawMarker(test_image1, (int(center_label[0]), int(center_label[1])), (0, 200, 0), markerType=cv2.MARKER_STAR, markerSize=10)

                # 緑円
#                cv2.circle(test_image1, (int(center_label[0]), int(center_label[1])), 3, (0, 200, 0), thickness=3)  # center label
#                cv2.circle(test_image1, (int(center_label[0]), int(center_label[1])), r_for_pointing, (0, 200, 0), thickness=1)

                # text
                font = cv2.FONT_HERSHEY_SIMPLEX
#                cv2.putText(test_image1, "potC:softmax: " + str(accu[0][0][0][1]), (15, 15), font, 0.5, (255, 0, 255), 1, cv2.LINE_AA)
       #         cv2.rectangle(test_image1,(10,12),(300,20),(255, 255, 255), cv2.FILLED)
#                cv2.putText(test_image1, 'center', (int(center_label[0]), int(center_label[1])), font, 0.6, (0, 200, 0), 1, cv2.LINE_AA)

                # 出力値が0.7より高い場合サークルを表示
                print('accu[0][0][0][1]',accu[0][0][0][1])
                if accu[0][0][0][1] > 0.4:


                    from Modules import utils

                    import skimage.transform
                    est_map_np_origsize = \
                        skimage.transform.resize(det[:, :, 1:2],
                                                 output_shape=(405, 720),
                                                 mode='constant')

                    mask, _ = utils.threshold(est_map_np_origsize, 0.5)
                    center_point = utils.cluster(mask, 1)
                    # cv2.circle(test_image1, (center_point[0],center_point[1]), 3, (255, 10, 0), thickness=3)
                    utils.paint_circles(test_image1, points=center_point)

                    from Modules import utils

                    import skimage.transform
                    est_map_np_origsize = \
                        skimage.transform.resize(locate_[:,:,1:2],
                                                 output_shape=(405, 720),
                                                 mode='constant')

                    mask, _ = utils.threshold(est_map_np_origsize, 0.5)
                    center_point = utils.cluster(mask, 1)
                    # cv2.circle(test_image1, (center_point[0],center_point[1]), 3, (255, 10, 0), thickness=3)
                    utils.paint_circles(locate_, points=center_point)


                    # サークル(中心座標)の表示
                    cv2.circle(det, (posy, posx), 3, (20, 20, 2), thickness=3)  # 赤  regression model 予測
                    cv2.circle(det, maloc, 3, (255, 0, 255), thickness=3)  # ピンク ヒートマップ最大値
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    cv2.putText(det, 'reg', (posy, posx), font, 0.6, (255, 255, 255), 2, cv2.LINE_AA)
                    cv2.putText(det, "max", maloc, font, 0.6, (255, 0, 255), 1, cv2.LINE_AA)

                    cv2.circle(test_image1, (posy, posx), 3, (255, 10, 0), thickness=3)  # 赤  regression model 予測
                    cv2.circle(test_image1, maloc, 3, (255, 0, 255), thickness=3)  # ピンク ヒートマップ最大値

                    cv2.putText(test_image1, 'reg', (posy, posx), font, 0.6, (255, 10, 0), 1, cv2.LINE_AA)
                    cv2.putText(test_image1, 'max', maloc, font, 0.6, (255, 0, 255), 1, cv2.LINE_AA)

                    if (posx-center_label[0])**2 + (posy-center_label[1])**2 <= r**2:
                        #print("reg-衝突")
                        tp+=1
                    else:
                        tn+=1
                        #print("reg-外")

                    if (maloc[0] - center_label[0]) ** 2 + (maloc[1] - center_label[1]) ** 2 <= r**2:
                        tp_max+=1
                        #print('max=衝突')
                    else:
                        tn_max+=1
                        #print('max=外')
                else:
                    if (posx - center_label[0]) ** 2 + (posy - center_label[1]) ** 2 <= r**2:
                        fp+=1
                    else:
                        fn+=1

                    if (maloc[0] - center_label[0]) ** 2 + (maloc[1] - center_label[1]) ** 2 <= r**2:
                        fp_max+=1
                    else:
                        fn_max+=1
                original_image = cv2.resize(original_image, (576, 324))
                cv2.imshow('ori',original_image)
                locate_ = cv2.resize(locate_, (576, 324))
                cv2.imshow('locate',locate_[:,:,1:2])

                test_image1 = cv2.resize(test_image1,(576, 324))
                # テスト画像の表示
                cv2.imshow("input_img", test_image1)
            #   cv2.imshow("input_img_for_accu", test_image_for_accuracy)
                # cv2.imwrite("./video/" + str(num)+"test.jpg", test_img)
                #                    out.write(test_img)

               # print("MAX" + str(maloc))

                # center_point = input_image_size/2
                # RMSE_X_MAX = np.sqrt(np.mean((center_point-maloc[0])**2))
                # RMSE_Y_MAX = np.sqrt(np.mean((center_point-maloc[1])**2))
                # RMSE_X_OUT = np.sqrt(np.mean((center_point-posx)**2))
                # RMSE_Y_OUT = np.sqrt(np.mean((center_point-posy)**2))
                # RMSE_X = (RMSE_X_MAX + RMSE_X_OUT) /2
                # RMSE_Y = (RMSE_Y_MAX + RMSE_Y_OUT) /2
                # RMSE = (RMSE_X + RMSE_Y) /2
                # print("RMSE_X:"+str(RMSE_X)+"RMSE_Y:"+str(RMSE_Y)+"RMSE_XY:" + str(RMSE))

                num += 1
                # ヒートマップの表示
                #det = cv2.resize(det, (360, 280))
                cv2.imshow("final_layer_conv5_heatmap", det[:, :, 1:2])
           #     cv2.imwrite('/Users/matsunagamasaaki/MasterResearch/annotation_for_reg/data/heat_data/'+str(num)+'.jpg',det[:, :, 1:2]*256)
                # cv2.imwrite("./video_det/" + str(num) + "det.jpg", det)
                #                    out.write(det)
                cv2.waitKey(1)
            try:
                precision = tp/(tp+fp)
                precision_max = tp_max / (tp_max + fp_max)
                recall = tp / (tp + fn)
                recall_max = tp_max / (tp_max + fn_max)
                f_score = 2 * (precision * recall) / (precision + recall)
                f_score_max = 2 * (precision_max * recall_max) / (precision_max + recall_max)

            except ZeroDivisionError:
                precision=0
                precision_max=0
                f_score=0
                f_score_max = 0

            else:
                precision = tp / (tp + fp)
                precision_max = tp_max / (tp_max + fp_max)
                recall = tp / (tp + fn)
                recall_max = tp_max / (tp_max + fn_max)
                f_score = 2 * (precision * recall) / (precision + recall)
                f_score_max = 2 * (precision_max * recall_max) / (precision_max + recall_max)

            print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            print("precision: ",precision)
            print("precision_max: ", precision_max)

            print("recall", recall)
            print("recall_max", recall_max)

            print("f_score: ", f_score)
            print("f_socre_max: ", f_score_max)



            """
            summary_writer = tf.summary.FileWriter(REG_LOG_DIR, sess.graph)

            with tf.name_scope('summary'):

                tf.summary.scalar('precision_reg', precision)
                tf.summary.scalar('precision_max', precision_max)
                #            tf.summary.scalar('validation_loss', loss_valid)
                tf.summary.scalar('recall_reg', recall)
                tf.summary.scalar('recall_max', recall_max)

                merged = tf.summary.merge_all(name='regression')
                
            """

            return f_score, f_score_max, precision, precision_max, recall, recall_max


class Tester:
    def __init__(self):
        pass
        #self.initializer = ones

    def tensorToRGB(self,tensor):
        if tensor.ndim == 2 or tensor.shape[2] == 1:
            col = np.ndarray(shape=[tensor.shape[0], tensor.shape[1], 3], dtype=tensor.dtype)
            col[:,:,0:1] = tensor
            col[:,:,1:2] = tensor
            col[:,:,2:3] = tensor
            return col
        elif tensor.shape[2] == 3:
            return cv2.cvtColor(tensor, cv2.COLOR_BGR2RGB)
        elif tensor.shape[2] == 2:
            zeros = np.zeros(shape=(tensor.shape[0], tensor.shape[1], 1), dtype=np.float32)
            return np.append(tensor, zeros, axis=2)
        else:
            print("invalid tensor format for showing")
            exit()

    def show2(self, batcha, batchb):
        resx = [self.tensorToRGB(i) for i in batcha]
        rest = [self.tensorToRGB(i) for i in batchb]
        cv2.imshow("show", cv2.hconcat([cv2.vconcat(resx), cv2.vconcat(rest)]))
#        cv2.waitKey(wait)

    def show(self, batcha, pos, test_image, name="show", show_max=-1):
        print("type",type(batcha))
        print("shape",np.shape(batcha))
        resx = [self.tensorToRGB(i) for i in batcha]

        if show_max >= 0:
            for img in resx:

                    index_max = np.argmax(img[:, :, show_max:show_max+1])
                    x, y = (index_max % img.shape[1], index_max / img.shape[1])
                    print(index_max, img.shape, x, y)
                    cv2.circle(img, (int(x),int(y)), 5, (1, 1, 1))

                    #img = self.tensorToRGB(img)
                    post = list(pos[0])

                    posy = int(post[0]* img.shape[0])
                    posx = int(post[1]* img.shape[1])
                    print("pos", (posx, posy))
                    cv2.circle(img, (posx, posy), 3, (0, 0, 255), thickness=1)  # 赤  regression model 予測

        a = cv2.resize(cv2.vconcat(resx), (360, 280)) #(360,405))#

        cv2.imshow(name, a)
     #   cv2.waitKey(1)

    def show_tile(self,batch, name="show"):

        ilist = [self.tensorToRGB(i) for i in batch]

        w = round(math.sqrt(batch.shape[0]) + 0.5)
        h = round(batch.shape[0] / w + 0.4)
        sh = ilist[0].shape
        extra = [np.zeros(shape=sh, dtype=ilist[0].dtype) for _ in range(w*h-len(ilist))]
        ilist.extend(extra)

        tile = []
        for r in range(0, w * h, w):
            row = []
            for c in range(w):
                row.append(ilist[r + c])

            tile.append(cv2.hconcat(row))

        #print(cv2.vconcat(tile).shape)
        tile_ = cv2.resize(cv2.vconcat(tile), (900, 700))
        cv2.imshow(name, tile_)

    def show_intermediate_heatmap(self,sess,intermediate_output,test_input_image,img):

        imgg = sess.run(intermediate_output, feed_dict={test_input_image:img})
        row = np.array(imgg)[0, 0, :, :, 0:1]
    #    print("original", np.shape(row))
        row_ = []
        final=0
        count = 0
        for h in range(1, 32):
            imgg_ = np.array(imgg)[0, 0, :, :, h:h + 1]
            row = np.hstack((row, imgg_))
     #       print("row", np.shape(row))
            if h % 4 == 0:
                row_.append(row)
                row = imgg_
                if count == 0:
                    final = row_[0]
                if count > 0:
                    final = np.vstack((final, row_[count]))
      #              print("final", np.shape(final))
                count += 1

        # print("final",np.shape(final))

        print("intermediate", np.shape(row_[3]))

        resized_imgg_ = cv2.resize(final, (470, 700))  # (470,1176))#(672,168))
        cv2.imshow("intermediate", resized_imgg_)
        cv2.waitKey(1)

    def to_label(self, img, shape):
        gt_ = cv2.resize(img, shape, None, 0, 0, cv2.INTER_AREA)
        gt_ = gt_[:, :, 0:1]
        ngt = 1 - gt_
        gt_ = np.append(gt_, ngt, axis=2)

        return gt_

    def run(self):

        # tf.reset_default_graph()
        temp_cvs = "temp_train.csv"

        pipeline = NNPipeline()
        pipeline.train(TRAIN_FILE, VALID_FILE)

    def run_reg(self):
        pipleline = NNPipeline()
        pipleline.run_regression()

    def run_test(self,radius,r):
        pipeline = NNPipeline()
        pipeline.run_test(TEST_FILE,radius,r)

    def run_test_for_metric_evaluation(self, r):

        import matplotlib.pyplot as plt
        from tqdm import tqdm
        import pandas as pd

        f_reg = []
        f_max = []
        precision_reg = []
        precision_max_ = []
        recall_reg = []
        recall_max_ = []

        r_range = (20, 71)

        for i in tqdm(range(r_range[0], r_range[1])):
            tf.reset_default_graph()

            pipleline = NNPipeline()

            freg, fmax, precision, precision_max, recall, recall_max = pipleline.run_test(TEST_FILE, i)

            plt.xlabel('r (in pixels)')
            plt.ylabel('f score')
            f_reg.append(freg)
            f_max.append(fmax)
            precision_reg.append(precision)
            precision_max_.append(precision_max)
            recall_reg.append(recall)
            recall_max_.append(recall_max)

        df = pd.DataFrame({'f_reg': f_reg, 'f_max': f_max, 'precision': precision_reg,
                           'precision_max': precision_max_, 'recall': recall_reg, 'recall_max': recall_max_
                              , 'r': [i for i in range(r_range[0], r_range[1])]})

        df.to_csv('f_score.csv')
        plt.grid(True, lw=2, ls='--', c='.75')
        plt.plot([i for i in range(r_range[0], r_range[1])], f_reg)
        plt.plot([i for i in range(r_range[0], r_range[1])], f_max)
        plt.savefig('f_score.jpg')

#        return freg, fmax, precision, precision_max, recall, recall_max


if __name__ == '__main__':

    TRAIN_FILE = Switch(0).TRAIN_FILE
    VALID_FILE = Switch(0).VALID_FILE
    TEST_FILE = Switch(0).TEST_FILE
    print(TRAIN_FILE)

  #  Tester().run_reg()
    #Tester().run()
    Tester().run_test(50, 50)

    #Tester().run_test_for_metric_evaluation(i)




