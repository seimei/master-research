<html>
<body>
<div>
  <h1>Master Research</h1>
  <img alt="er" src="/images/method.png">
</div>

<div>
<h2>Research Objective</h2>
<ul>
<li>博物館等の多数展示品を対象に物体検出を行う場合、全ての品に対してNNモデルの学習に十分なラベルの取得を行うことは現実的でない。</li>
<li>①　故に少ないデータセットで十分精度が高い物体検出モデルの構築を実現する必要がある。</li>
<li>加えて現在存在する最も精度が高い物体検出モデルでは、Boundding Boxでの学習、および検出を行うものが主要であり。</li>
<li>この事が原因となり、計算コストが高くなることからモデルのサイズも必然的に大きくなる傾向がある。</li>
<li>②　故にBounding Boxでの学習・推論を行わず、ポイントで対象物体座標を推定するモデルの開発を行う。</li>
</ul>
</div>

<div>
<h2>Class Category of Target Objects</h2>
  <img alt="er" src="/images/exhibits.png">
</div>



</div>

<div>
<h2>Directories/Files</h2>
<ul>
<li>main.py- tain/testの実行コード</li>
<li>Network/one_shot_detector_mymodel.py- 分類CNN/回帰CNNのnetwork構造</li>
<li>InputPipline/DistTest.py- input pipline</li>
<li>create_labeled_file_train.py- path_and_label_train.csvを作成するスクリプト</li>
<li>create_labeled_file_valid.py- path_and_label_valid.csvを作成するスクリプト</li>
<li>create_labeled_file_test.py-  paht_and_label_testを作成するスクリプト</li>
<li>path_and_label_train.csv- 分類CNNの学習に使用されるcsvファイル</li>
<li>path_and_label_valid.csv- 分類CNNの評価に使用されるcsvファイル</li>
<li>paht_and_label_test.csv- 分類CNN/回帰CNNの中心座標検出の評価に使用されるcsvファイル</li>
<li>analyse-training-dataset-using-TSNE.ipynb- 分類CNNの学習データセットにおける分析を目的としたTSNEの適用</li>
<li>crop_background_from_video.ipynb- 動画より背景をrandomに切り出すコード</li>

</ul>
</div>

<div>
<h2> Dependencies </h2>
<ul>
<li>Python 3.6.9 :: Anaconda, Inc.</li>
<li>tensorflow-gpu==1.14.0</li>
<li>numpy==1.17.0</li>
<li>opencv-python==4.1.0.25</li>	
</ul>
</div>

</body>
</html>
